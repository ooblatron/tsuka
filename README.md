# Tsuka - Create Bootstrap web pages with pure PHP and no templates

Tsuka allows you to create beautiful web pages using the Bootstrap framework without writing any HTML or CSS.

## Why Tsuka?

Tsuka is an alternative to templating engines like [Twig](https://twig.symfony.com/) and [Blade](https://laravel.com/docs/9.x/blade). These are great for creating web pages, but require knowledge of HTML and CSS. It is also quite fiddly coding in all the relevant tags. Tsuka allows you to build webpages by calling easy-to-use methods which map to underlying Bootstrap functionality, e.g.

- addParagraph()
- addList()
- addNavBar()
- addLead()
- addDropdown()
 
and many more.

You can use PHP loops, or anything else, as Tsuka is pure PHP. There is no need to learn a special templating loop syntax or how to embed PHP into a template. With Tsuka, your view classes are pure PHP.

You can also easily upgrade to a new version of Bootstrap, as the underlying methods will stay the same. Even things like Jumbotrons, which have been removed from Bootstrap 5, will continue to be supported by Tsuka using the addJumbotron() method.

## Installation

Install the latest version with

```bash
$ composer require ooblatron/tsuka
```

## Basic Usage

```php
<?php

use Tsuka\Bootstrap4;

class HelloWorldViewport extends Bootstrap4
{
    public function build() {
        $this->startPage('Page title');
        $this->addH1('Hello world');
        $this->endPage();
        return $this;
    }
}
```

Echo the output.

### Basic example

```php
<?php

$viewport = new HelloWorldViewport();
$viewport->build()->display();
```

### Laravel

```php
<?php

class SomeController
{
.
.
.
    public function someMethod(
        HelloWorldViewport $viewport    
    ) {
        return $viewport->build()->getContent();
    }
}
```

### [Slipstream](https://packagist.org/packages/ooblatron/slipstream)

```php
<?php

class SomeController
{
    public function __construct(
        HelloWorldViewport $viewport    
    ) {
        $viewport->build()->display();
    }
}
```

## Documentation

- [Please visit the Tsuka website](https://tsuka.ooblatron.org)

## About

### Requirements

- PHP 7.1 or above
- Composer package [vlucas/valitron](https://packagist.org/packages/vlucas/valitron) for form validation

### Bootstrap versions

- Version 1 of Tsuka supports Bootstrap 4.

### Author

Tim Rogers - <tim@ooblatron.org> - <https://twitter.com/timdrogers><br />

### License

Tsuka is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

### Acknowledgements

This package uses the brilliant [Bootstrap](https://getbootstrap.com/) framework, which allows back-end coders to create clean and professional web pages without needing a deep knowledge of graphic design or CSS.