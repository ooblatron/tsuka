### 1.0.0 (2022-11-24)

* Initial release

### 1.0.1 (2022-11-30)

* Fixed error when DebugBar facade returns null inner DebugBar

### 1.0.2 (2022-12-01)

* Fixed persistence of second anti-CSRF token

### 1.0.3 (2022-12-02)

* Fixed addReportTitleAndDownloadButton()
* Removed unused elements from Bootstrap class

### 1.0.4 (2022-12-02)

* Added missing return types
* Added startPage() and endPage()

### 1.1 (2022-12-05)

* Enhanced usage instructions in README.md
* Added simple test page
* Removed unused methods
* Renamed getter and other methods to be more meaningful and PSR-compliant

Viewport interface

| Old method name               | New method name                      |
|:------------------------------|:-------------------------------------|
| content()                     | getContent()                         |
| output_csv()                  | outputCsv()                          |
| convertToId()                 | getConvertedToId()                   |
| viewport()                    | getViewport()                        |
| unpackSafe()                  | getUnpackedSafe()                    |
| makeSafe()                    | getSafe()                            |
| linkSafe()                    | getLinkSafe()                        |
| link()                        | getLink()                            |
|
BootstrapForm trait

| Old method name | New method name |
|:----------------|:----------------|
| formTag()       | getFormTag()    |

Jquery interface

| Old method name | New method name |
|:----------------|:----------------|
| content()       | getContent()    |

### 1.1.1 (2022-12-07)

* Fixed Bootstrap tooltip styling

### 1.1.2 (2022-12-07)

* append() and prepend() methods can now accept Viewport objects

### 1.1.3 (2022-12-07)

* Fix prepend() method

### 1.1.4 (2022-12-08)

* startCardOverlay() can now handle images generated from files

### 1.1.5 (2022-12-08)

* Logo image can now be passed to addNavBarBrand()

### 1.1.6 (2022-12-08)

* Fixed home button broken when in a subdirectory

### 1.1.7 (2022-12-08)

* Changed documentation link in README.md to Tsuka microsite

### 1.1.8 (2022-12-08)

* Changed homepage in composer.json to Tsuka microsite

### 1.1.9 (2022-12-09)

* Fixed broken card overlay

### 1.1.10 (2022-12-09)

* Fixed malformed <HEAD> element

### 1.1.11 (2022-12-11)

* Added startContainer() and endContainer() viewport methods

### 1.1.12-RC (2022-12-14)

* Fixed homepage link on navbar