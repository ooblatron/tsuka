<?php

namespace Tsuka;

use DateTime;
use Carbon\Carbon;
use Tsuka\Interfaces\DebugBar;
use Exception;
use Tsuka\Interfaces\Jquery;
use Tsuka\Interfaces\HtmlEntities;
use Tsuka\Interfaces\Icons;
use Tsuka\Interfaces\Viewport;
use function array_merge;
use function count;
use function is_array;
use function is_null;
use function is_numeric;
use function is_string;
use function reset;
use const PHP_EOL;


/**
 * Class Viewport
 */
class Bootstrap implements Viewport, Icons, HtmlEntities
{
    protected const POST = 'POST';
    protected const GET = 'GET';
    protected const CHECKED = ' CHECKED';
    protected const DISABLED = ' DISABLED';

    private const HTML_LANGUAGE = 'en';
    // todo Allow child class constants to be added to whitelist
    private const WHITELIST = [
        self::ICON_HELP,
        self::ICON_SETTINGS,
        self::ICON_EDIT,
        self::ICON_EDIT_CLASS,
        self::ICON_PRINT,
        self::ICON_EMAIL,
        self::ICON_CALENDAR_CLASS,
        self::ICON_CALENDAR,
        self::ICON_REMOVE_CLASS,
        self::ICON_REMOVE,
        self::ICON_REMOVE_THIN,
        self::ICON_TICK,
        self::ICON_TICK_THIN,
        self::ICON_ADD,
        self::ICON_POSITION,
        self::ICON_TOGGLE_ON,
        self::ICON_TOGGLE_OFF,
        self::ICON_ARROW_UP,
        self::ICON_ARROW_DOWN,
        self::ICON_INSERT,
        self::ICON_INSERT_WHITE,
        self::ICON_INSERT_BELOW,
        self::ICON_QUESTION_MARK,
        self::ICON_DOCUMENT,
        self::ICON_TRANSFER,
        self::ICON_DOWNLOAD,
        self::ICON_LOUDHAILER,
        self::ICON_DELETE,
        self::ICON_BAN,
        self::ICON_FAST_FORWARD,
        self::ICON_REWIND,
        self::ICON_STEP_FORWARD,
        self::ICON_STEP_BACKWARD,
        self::ICON_POINTER,
        self::ICON_LOCATION,
        self::ICON_VIDEO,
        self::ICON_LISTEN,
        self::ICON_PARTY,
        self::ICON_EYE,
        self::ICON_COMMENT,
        self::ICON_COMMENT_OUTLINE,
        self::NBSP,
        self::NDASH,
        self::MDASH,
        self::CURRENCY,
        self::INFO_CIRCLE,
        self::APOSTROPHE,
        self::START_STRIKETHROUGH,
        self::END_STRIKETHROUGH,
        self::START_CODE,
        self::END_CODE,
        self::WHITESPACE
    ];

    protected const INPUT_TYPE_TEXT = 'text';

    private const QR_CODE_ERROR_CORRECTION_LEVEL = 'M';
    private const DT_DATE_SHORT = 'd M Y';


    /**
     * @var array 
     */
    public $header;
    /**
     * @var array 
     */
    public $report = array();
    /**
     * @var array 
     */
    public $viewport = array();
    /**
     * @var int 
     */
    private $navbarId = 0;
    /**
     * @var bool 
     */
    private $navbarBrandIsOpen = false;
    /**
     * @var array 
     */
    private $reportRow = array();
    /**
     * @var array 
     */
    private $reportCell;
    /**
     * @var string|array|null 
     */
    private $reportCellWidth;
    /**
     * @var int 
     */
    private $stripe = 0;

    /**
     * @param $_text
     * @param array|null|string $_input
     * @throws Exception
     */
    public function addParagraph(
        $_text,
        $_input = array()
    ): void {
        $input = $_input;
        if (is_string($_input)) {
            $input = ['class' => $_input];
        }
        $this->startParagraph($input);
        $this->viewport[] = $this->getSafe($_text);
        $this->startReportRow();
        $this->addReportCell($_text, null);
        $this->endReportRow();
        $this->endParagraph();
    }

    /**
     * @param array $_input
     */
    public function startParagraph(array $_input = [])
    {
        // Class defaults to empty string
        $class = !isset ($_input['class']) ? '' : ' class="' . $_input['class'] . '"';

        $this->viewport[] = "<p" . $class . '>';
    }

    /**
     *
     */
    public function endParagraph()
    {
        $this->viewport[] = "</p>";
    }

    /**
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    public function addH1(
        $_text,
        ?array $_input = array()
    ): void {
        $this->addHtmlHeader('h1', $_text, $_input);
    }

    /**
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    public function addH2(
        $_text,
        ?array $_input = array()
    ): void {
        $this->addHtmlHeader('h2', $_text, $_input);
    }

    /**
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    public function addH3(
        $_text,
        ?array $_input = array()
    ): void {
        $this->addHtmlHeader('h3', $_text, $_input);
    }

    /**
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    public function addH4(
        $_text,
        ?array $_input = array()
    ): void {
        $this->addHtmlHeader('h4', $_text, $_input);
    }

    /**
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    public function addH5(
        $_text,
        ?array $_input = array()
    ): void {
        $this->addHtmlHeader('h5', $_text, $_input);
    }

    /**
     * @param string $_header
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    private function addHtmlHeader(
        string $_header,
        $_text,
        ?array $_input
    ): void {
        $this->startHeader($_header, $_input);
        $this->viewport[] = $this->getSafe($_text);
        $this->startReportRow();
        $this->addReportCell($_text, null);
        $this->endReportRow();
        $this->endHeader($_header, $_input);
    }

    /**
     * @param $_header
     * @param $_input
     */
    public function startHeader($_header, $_input)
    {
        // Additional class attributes default to empty string
        $class = !isset ($_input['class']) ? '' : $_input['class'];

        // Card title defaults to FALSE
        $card_title = isset($_input['card_title']) && $_input['card_title'];

        if ($card_title) {
            if ($class == '') {
                $class = "card-title";
            } else {
                $class .= " card-title";
            }
        }

        $line = "<" . $_header;

        if ($class != '') {
            $line .= ' class="' . $class . '"';
        }

        $line .= '>';

        $this->viewport[] = $line;
    }

    /**
     * @param $_header
     * @param $_input
     * @throws Exception
     */
    public function endHeader($_header, $_input)
    {
        $line = '';

        if (isset($_input['small'])) {
            $line .= "<small>" . $this->getSafe($_input['small']) . "</small>";
        }

        $line .= "</" . $_header . '>';

        $this->viewport[] = $line;
    }

    /**
     *
     */
    public function addHr()
    {
        $this->viewport[] = "<hr/>";
    }

    /**
     * @param $_input
     * @param string $_width
     */
    public function addListGroup($_input, string $_width = '')
    {
        $width = !isset ($_width) ? '' : " style=max-width:" . $_width . "px";

        $this->viewport[] = "<ul class=\"list-group mb-3\"" . $width . '>';

        foreach ($_input as $line) {
            if ($line != '') {
                $this->viewport[] =
                    '<li class="list-group-item">' .
                    $line .
                    "</li>";
            }
        }

        $this->viewport[] = "</ul>";
    }

    /**
     * @param $_input
     * @throws Exception
     */
    public function addButton($_input)
    {
        // label
        // link				optional
        // colour			optional
        // block			optional
        // outline			optional
        // small			optional
        // mb				optional
        // submit			optional
        // class			optional
        // modal			optional
        // modal_close		optional
        // assistive_role	optional
        // tooltip			optional
        // id               optional

        # Colour defaults to primary
        $colour = !isset ($_input["colour"]) ? "primary" : $_input["colour"];

        # Block defaults to FALSE
        $block = !isset ($_input["block"]) ? false : $_input["block"];

        # Outline defaults to FALSE
        $outline = !isset ($_input["outline"]) ? false : $_input["outline"];

        # Small defaults to FALSE
        $small = !isset ($_input['small']) ? false : $_input['small'];

        # Submit defaults to false
        $submit = $_input["submit"] ?? false;

        # Class defaults to empty string
        $class = !isset ($_input['class']) ? '' : ' ' . $_input['class'];

        # modal defaults to empty string
        $modal = !isset ($_input["modal"]) ? '' :
            " data-toggle=\"modal\" data-target=\"#" . $_input["modal"] . '"';

        # modal_close defaults to empty string
        $modal_close = !isset ($_input["modal_close"]) ? '' :
            ($_input["modal_close"] ? " data-dismiss=\"modal\"" : '');

        # assistive_role defaults to empty string
        $assistive_role = !isset ($_input["assistive_role"]) ? '' :
            ($_input["assistive_role"] ? " role=\"button\"" : '');

        # id defaults to empty string
        $id = isset($_input['id']) ? ' id="'.$_input['id'].'"' : '';

        if (!empty($_input["link"])) {
            $line = "<a href=\"" . $_input["link"] . '"';
            $typeSuffix = '';
        } else {
            $line = '<button ';
            $typeSuffix = 'button';
        }

        $line .=
            " class=\"btn" .
            ($outline ? " btn-outline-" . $colour : " btn-" . $colour) .
            ($block ? " btn-block" : '') .
            ($small ? " btn-sm" : '') .
            $class .
            (isset ($_input["tooltip"]) ?
                " data-toggle=\"tooltip\" data-placement=\"top\"" .
                " title=\"" . $_input["tooltip"] . '"' : '') .
            (isset ($_input["mb"]) ?
                " mb-" . $_input["mb"] : '') .
            '"' .
            $modal .
            $modal_close .
            $assistive_role .
            $id;

        if ($submit) {
            $type = 'submit';
        } else {
            $type = '';
        }

        if (!empty($type.$typeSuffix)) {
            $line .= ' type="' . $type.(empty($type) ? '' : ' ') . $typeSuffix . '"';
        }

        $line .= '>' . $this->getSafe($_input["label"]);

        if (!empty($_input["link"])) {
            $line .= '</a>';
        } else {
            $line .= "</button>";
        }

        $this->viewport[] = $line;
    }

    /**
     * @param $_text
     * @param array $_input
     * @throws Exception
     */
    public function addAlert(
        $_text,
        array $_input = array()
    )
    {
        // Input
        //
        // dismissible	optional
        // type			optional
        // class        optional

        # dismissible defaults to FALSE
        $dismissible = !isset($_input["dismissible"]) ? false : $_input["dismissible"];

        # type defaults to success
        $type = !isset($_input["type"]) ? "success" : $_input["type"];

        # class defaults to empty string
        $class = !isset($_input['class']) ? '' : ' '.$_input['class'];

        $this->viewport[] =
            "<div" .
            ' class="' .
            "alert alert-" . $type .
            ($dismissible ? " alert-dismissible fade show" : '') .
            $class.
            '"' .
            " role=\"alert\"" .
            '>';

        if ($dismissible) {
            $this->viewport[] = '<button class="close"' .
                ' data-dismiss="alert"' .
                ' type="button">' .
                "<span>&times;</span>" .
                "</button>";
        }

        $this->viewport[] = $this->getSafe($_text);

        $this->viewport[] = "</div><!-- ./alert -->";
    }

    /**
     * @param $_text
     * @param null $_input
     * @throws Exception
     */
    public function addDiv(
        $_text,
        $_input = null
    )
    {
        $this->startDiv($_input);
        $this->viewport[] = $this->getSafe($_text);
        $this->endDiv();
    }

    /**
     * @param array|null $_input
     */
    public function startDiv(array $_input = null)
    {
        $class = $_input['class'] ?? '';
        $style = $_input['style'] ?? '';
        $id = $_input["id"] ?? '';

        // class, style and id default to empty string
        $class = $class == '' ? '' : ' class="' . $class . '"';
        $style = $style == '' ? '' : ' style="' . $style . '"';
        $id = $id == '' ? '' : ' id="' . $id . '"';

        $this->viewport[] = "<div" . $class . $id . $style . '>';
    }

    /**
     *
     */
    public function endDiv()
    {
        $this->viewport[] = "</div>";
    }

    /**
     * @param $_text
     * @param string $_class
     * @throws Exception
     */
    public function addSpan(
        $_text,
        string $_class = ''
    )
    {
        $this->startSpan($_class);
        $this->viewport[] = $this->getSafe($_text);
        $this->endSpan();
    }

    /**
     * @param string $_class
     */
    public function startSpan(string $_class = '')
    {
        // Class defaults to empty string
        $class = $_class == '' ? '' : ' class="' . $_class . '"';

        $this->viewport[] = "<span" . $class . '>';
    }

    /**
     *
     */
    public function endSpan()
    {
        $this->viewport[] = "</span>";
    }

    /**
     * @param $_text
     * @param string $_class
     * @return array
     * @throws Exception
     */
    public function span(
        $_text,
        string $_class = ''
    ): array {
        $class = $_class == '' ? '' : ' class="' . $_class . '"';
        $span = "<span" . $class . '>' . $this->getSafe($_text) . "</span>";

        return [true, $span];
    }

    /**
     * @param string $_width
     * @param string $_padding
     */
    public function startCardGroup(string $_width = '', string $_padding = '2')
    {
        // width defaults to empty string
        $width = $_width == '' ? '' : ' w-' . $_width;

        $this->viewport[] = '<div class="card-group pb-' . $_padding . $width . '">';
    }

    /**
     *
     */
    public function endCardGroup()
    {
        $this->viewport[] = '</div><!-- ./card_group -->';
    }

    /**
     *
     */
    public function startCardColumns()
    {
        $this->viewport[] = '<div class="card-columns">';
    }

    /**
     *
     */
    public function endCardColumns()
    {
        $this->viewport[] = "</div><!-- ./card_columns -->";
    }

    /**
     * @param array $_input
     */
    public function startCard(array $_input = array())
    {
        $class = isset($_input['class']) ? ' ' . $_input['class'] : '';
        $style = isset($_input["style"]) ? " style=\"" . $_input["style"] . '"' : '';
        $this->viewport[] = "<div class=\"card" . $class . '"' . $style . '>';
    }

    /**
     * End Card
     */
    public function endCard()
    {
        $this->viewport[] = '</div><!-- ./card -->';
    }

    public function startCardImage(
        string $_image,
        ?string $_alt = ''
    ): void
    {
        $this->viewport[] = '<img src="' . $_image . '" class="card-img" alt="' . $_alt .'">';
        $this->viewport[] = '<div class="card-img-overlay">';
    }

    public function endCardImage(): void
    {
        $this->viewport[] = '</div><!-- ./cardImage -->';
    }

    /**
     *
     */
    public function startCardBody()
    {
        $this->viewport[] = '<div class="card-body">';
    }

    /**
     *
     */
    public function endCardBody()
    {
        $this->viewport[] = '</div><!-- ./card_body -->';
    }

    public function startCardOverlay(
        string $_imagePath,
        ?string $_altText = null
    ): void {
        $this->addImage($_imagePath, $_altText, ['class' => 'card-img']);
        $this->startDiv(['class' => 'card-img-overlay']);
    }

    public function endCardOverlay(): void {
        $this->viewport[] = '</div><!-- /card_overlay -->';
    }

    /**
     * @param $_text
     * @param array $_input
     * @throws Exception
     */
    public function addCardTitle(
        $_text,
        array $_input = array()
    ): void {
        $_input['card_title'] = true;
        $this->addH5($_text, $_input);
    }

    /**
     * @param array $_input
     */
    public function startCardTitle(array $_input = array())
    {
        $_input['card_title'] = true;

        $this->startHeader('h5', $_input);
    }

    /**
     * @param array $_input
     * @throws Exception
     */
    public function endCardTitle(array $_input = array())
    {
        $_input['card_title'] = true;
        $this->endHeader('h5', $_input);
    }

    /**
     * @param $_text
     */
    public function addCardHeader(
        $_text
    )
    {
        $this->viewport[] = '<div class="card-header">';
        $this->viewport[] = $_text;
        $this->viewport[] = '</div><!-- ./card_footer -->';
    }

    /**
     * @param $_text
     * @throws Exception
     */
    public function addCardFooter(
        $_text
    ): void {
        $this->viewport[] = '<div class="card-footer">';
        $this->addSmall($_text);
        $this->viewport[] = '</div><!-- ./card_footer -->';
    }

    /**
     * @param string $_link
     * @param $_text
     * @param string $_class
     * @throws Exception
     */
    public function addCardLink(
        string $_link,
        $_text,
        string $_class = ''
    ): void {
        // class defaults to empty string
        $class = $_class == '' ? '' : ' ' . $_class;

        $this->viewport[] = '<a href="' . $_link . '" class="card-link' . $class . '">' .
            $this->getSafe($_text) .
            '</a>';
    }

    /**
     * @param $_text
     * @param string|null $_class
     * @throws Exception
     */
    public function addCardText(
        $_text,
        ?string $_class = null
    ): void {
        $class = 'card-text';
        if ($_class) {
            $class .= ' '. $_class;
        }
        $this->addParagraph($_text, ['class' => "' . $class . '"]);
    }

    /**
     * @param $_text
     * @param bool $_muted
     * @throws Exception
     */
    public function addSmall(
        $_text,
        bool $_muted = true
    ): void {
        $line = '<small';
        if ($_muted) {
            $line .= " class=\"text-muted\"";
        }
        $line .= '>';
        $line .= $this->getSafe($_text);
        $line .= '</small>';
        $this->viewport[] = $line;
    }

    /**
     * @param $_input
     * @param bool $_inline
     * @throws Exception
     */
    public function addInlineInputText($_input, bool $_inline = true)
    {
        //	Inputs
        //
        //	placeholder optional
        //	readonly    optional
        //	type        optional
        //	name        optional
        //	value       optional
        //	id			optional
        //	label		optional
        //	label-cols	optional
        //	input-cols	optional
        //	label-class	optional
        //	class		optional
        //  is-invalid  optional

        # readonly defaults to FALSE
        $readonly = !isset($_input["readonly"]) ? false : $_input["readonly"];

        # placeholder text defaults to empty string
        $placeholder_text = !isset($_input["placeholder"]) ? '' : $_input["placeholder"];

        # input type defaults to text
        $type = !isset($_input["type"]) ? self::INPUT_TYPE_TEXT : $_input["type"];

        # name_value defaults to empty string
        $name_value = !isset($_input["name"]) ? '' : " name=\"" . $_input["name"] . '"';

        # value_value defaults to empty string
        $value_value = !isset($_input["value"]) ? '' : " value=\"" . $_input["value"] . '"';

        # valid defaults to empty string
        if (is_null($_input['isInvalid'] ?? null)) {
            $valid = '';
        } else {
            $valid = $_input['isInvalid'] ? ' is-invalid' : ' is-valid';
        }

        if (isset ($_input["label"])) {
            # id defaults to label

            $id = !isset($_input["id"]) ?
                $this->getConvertedToId($this->getSafe($_input["label"])) :
                $_input["id"];

            $class = '';

            if ($_inline) {
                $class = "col-sm-" .
                    ($_input['label-cols'] ?? 2) .
                    " col-form-label";
            }

            if (isset ($_input["label-class"])) {
                $class .= ' ' . $_input["label-class"];
            }

            $this->viewport[] = '<label for="' . $id . '" ' .
                'class="' . $class . '">' .
                $_input["label"] .
                '</label>';

            if ($_inline and !isset ($_input["input-cols"])) {
                $this->startDiv(['class' => "col-sm-10"]);
            }
        } else {
            # id defaults to empty string

            $id = !isset($_input["id"]) ? '' : $_input["id"];
        }

        $id_value = " id=\"" . $id . '"';

        if (isset ($_input["input-cols"])) {
            $this->startDiv(['class' => "col-sm-" . $_input["input-cols"]]);
        }


        if ($type != "file") {
            $form_control = "form-control";
        } else {
            $form_control = "form-control-file";
        }

        $class_value = ' class="' . $form_control . " form-control-md" . $valid;

        if (isset ($_input['class'])) {
            $class_value .= ' ' . $_input['class'];
        }

        $class_value .= '"';

        $this->viewport[] = "<input" .
            " type=\"" . $type . '"' .
            $name_value .
            $value_value .
            $class_value .
            " placeholder=\"" . $this->getSafe($placeholder_text) . '"' .
            $id_value .
            ($readonly ? " READONLY" : '') . '>';

        if ($_inline and (isset ($_input["label"]) or
                isset ($_input["input-cols"]))) {
            $this->endDiv();
        }
    }

    /**
     * Start Form Row
     * @param string|null $_class
     */
    public function startFormRow(?string $_class = null)
    {
        $class = "form-row";

        if (isset ($_class)) {
            $class .= ' ' . $_class;
        }

        $this->startDiv(['class' => $class]);
    }

    /**
     *
     */
    public function endFormRow()
    {
        $this->viewport[] = '</div><!-- ./form-row -->';
    }

    /**
     * @param null $_class
     */
    public function startTable($_class = null)
    {
        // class	optional

        $class = !isset ($_class) ? '' : ' ' . $_class;

        $this->viewport[] = '<table class="table' . $class . '">';
    }

    /**
     *
     */
    public function endTable()
    {
        $this->viewport[] = '</table>';
    }

    /**
     *
     */
    public function startTableRow()
    {
        $this->viewport[] = '<tr>';
    }

    /**
     *
     */
    public function endTableRow()
    {
        $this->viewport[] = '</tr>';
    }

    /**
     * @param $_text
     * @param string|null $_class
     * @param string|null $_colspan
     * @param string|null $_attribute
     * @throws Exception
     */
    public function addTableCell(
        $_text,
        ?string $_class = '',
        ?string $_colspan = '',
        ?string $_attribute = ''
    ): void
    {
        $this->startTableCell($_class, $_colspan, $_attribute);
        $this->viewport[] = $this->getSafe($_text);
        $this->endTableCell();
    }

    /**
     * Start Table Cell
     * @param string|null $_class
     * @param string|null $_colspan
     * @param string|null $_attribute
     */
    public function startTableCell(
        ?string $_class = '',
        ?string $_colspan = '',
        ?string $_attribute = ''
    )
    {
        $class = $_class == '' ? '' : ' class="' . $_class . '"';
        $colspan = $_colspan == '' ? '' : " COLSPAN=\"" . $_colspan . '"';
        $attribute = $_attribute == '' ? '' : ' ' . $_attribute;

        $this->viewport[] = "<td" . $class . $colspan . $attribute . '>';
    }

    /**
     *
     */
    public function endTableCell()
    {
        $this->viewport[] = "</td>";
    }

    /**
     * Prepend
     * Prepends the passed viewport to the current viewport. The passed value can either be a viewport object, or a data
     * array that has been extracted from a viewport object.
     *
     * @param $_viewport array|Viewport
     * @return Viewport
     */
    public function prepend($_viewport): Viewport
    {
        if ($_viewport instanceof Viewport) {
            $viewportData = $_viewport->getViewportData();
        } else {
            $viewportData = $_viewport;
        }
        $this->viewport = array_merge($viewportData, $this->viewport);
        return $this;
    }

    /**
     * Append
     * Appends the passed viewport to the current viewport. The passed value can either be a viewport object, or a data
     * array that has been extracted from a viewport object.
     *
     * @param $_viewport array|Viewport
     * @return Viewport
     */
    public function append($_viewport): Viewport
    {
        if ($_viewport instanceof Viewport) {
            $viewportData = $_viewport->getViewportData();
        } else {
            $viewportData = $_viewport;
        }
        $this->viewport = array_merge($this->viewport, $viewportData);
        return $this;
    }

    /**
     * Append Once
     * Appends the passed viewport to the current viewport and clears the content of the passed viewport
     * @param Viewport $_viewport
     * @return $this
     */
    public function appendOnce(Viewport $_viewport): Viewport
    {
        $this->append($_viewport->getViewportData());
        $_viewport->clear();
        return $this;
    }

    /**
     * @param string $_url
     * @param $_text
     * @param array|null $_input
     * @throws Exception
     */
    public function addLink(
        string $_url,
        $_text,
        ?array $_input = []
    ): void
    {
        $this->viewport[] = $this->getLink($_url, $_text, $_input);
        $this->reportCell [] = $_text;
    }

    /**
     * Add Link Safe
     * @param string $_url
     * @param $_text
     * @param array|null $_input
     */
    public function addLinkSafe(
        string $_url,
        $_text,
        ?array $_input = []
    ): void
    {
        $this->viewport[] = $this->getLinkSafe($_url, $_text, $_input);
    }

    /**
     * Link
     * @param string $_url
     * @param $_text
     * @param array|string|null $_input
     * @return array
     * @throws Exception
     */
    public function getLink(
        string $_url,
        $_text,
        $_input = array()
    ): array
    {
        if (is_string($_input)) {
            $_input = ['class' => $_input];
        }

        return $this->getLinkSafe($_url, $this->getSafe($_text), $_input);
    }

    /**
     * @param string $_url
     * @param $_text
     * @param ?array $_input
     * @return array
     */
    public function getLinkSafe(
        string $_url,
        $_text,
        ?array $_input = []
    ): array
    {
        // class	optional
        // modal	optional

        // class defaults to empty string
        $class = !isset ($_input['class']) ? '' : ' class="' . $_input['class'] . '"';

        // modal defaults to FALSE
        $modal = isset ($_input["modal"]) && $_input["modal"];

        if (!$modal) {
            $link = '<a href="' . $_url . '" ' . $class . '>' . $_text . '</a>';
        } else {
            $link = '<a href="#" data-toggle="modal" data-target="#' .
                $_url .
                '" ' .
                $class .
                '>' .
                $_text .
                '</a>';
        }

        return [true, $link];
    }

    public function startLink(
        string $_link,
        ?string $_class = null
    ): void
    {
        $class = '';
        if ($_class) {
            $class = ' class="'.$_class.'"';
        }
        $this->viewport[] = '<a href="'.$_link.'"'.$class.'>';
    }

    public function endLink(): void
    {
        $this->viewport[] = '</a>';
    }

    /**
     * Start Grid
     * @param string|null $_width
     * @param string|null $_class
     */
    public function startGrid(
        ?string $_width = null,
        ?string $_class = null
    ): void {
        $style = !$_width ? null : ' max-width:' . $_width . 'px';
        $class = !$_class ? null : ' ' . $_class;
        $this->startContainer(
            $class,
            $style
        );
        $this->stripe = 0;
    }

    /**
     * End Grid
     */
    public function endGrid(): void {
        $this->endContainer('grid');
        if ($this->reportRow != array()) {
            $this->endReportRow();
        }
    }

    /**
     * Start Grid Row
     * @param string|null $_class
     */
    public function startGridRow(?string $_class = null)
    {
        $class = !$_class ? '' : ' ' . $_class;
        $this->stripe++;
        $this->viewport[] = '<div class="row' . $class . '">';
        $this->startReportRow();
    }

    public function startGridRowStriped(?string $_class = null) {
        $striped = $this->stripe % 2 ? null : ($_class ? ' ' : '') . 'bg-light';
        $class = !$_class ? '' : ' ' . $_class;
        if ($striped) {
            $class .= $striped;
        }
        $this->startGridRow($class);
    }

    /**
     *
     */
    private function startReportRow()
    {
        if ($this->reportRow != array()) {
            $this->endReportRow();
        }
    }

    /**
     *
     */
    public function endGridRow()
    {
        $this->viewport[] = "</div><!-- ./grid_row -->";
        $this->endReportRow();
    }

    private function endReportRow()
    {
        if ($this->reportRow != array()) {
            $this->report [] = $this->reportRow;
            $this->reportRow = array();
        }
    }

    /**
     * @param $_text
     * @param $_width
     * @param ?string $_class
     * @param ?string $_screenWidth
     * @throws Exception
     */
    public function addGridCell(
        $_text,
        $_width = null,
        ?string $_class = null,
        ?string $_screenWidth = null
    ): void
    {
        $this->startGridCell($_width, $_class, $_screenWidth);
        $this->viewport[] = $this->getSafe($_text);
        $this->endGridCell();
        $this->addReportCell($_text, $_width ?? null);
    }

    /**
     * @param $_text
     * @param string|array|null $_width
     * @throws Exception
     */
    public function addReportCell(
        $_text,
        $_width = '1'
    )
    {
        $data = $this->getSafe($_text);

        if (is_array($data)) {
            foreach ($data as $element) {
                $this->reportRow [] =
                    html_entity_decode(strip_tags($element));
            }
        } else {
            $cell = html_entity_decode(strip_tags($data ?? ''));

            if (DateTime::createFromFormat(self::DT_DATE_SHORT, $cell)) {
                $cell = Carbon::parse($cell)->toDateString();
            }

            $this->reportRow [] = $cell;
        }

        $max = is_array($_width) ? reset($_width) : $_width;
        if (is_numeric($max)) {
            for ($i = 1; $i < $max; $i++) {
                $this->reportRow [] = null;
            }
        }
    }

    /**
     * @param string|array|null $_width
     * @param string|null $_class
     * @param string|null $_screenWidth
     */
    public function startGridCell(
        $_width = null,
        ?string $_class = null,
        ?string $_screenWidth = null
    ): void
    {
        if (!is_array($_width)) {
            $divWidth = is_null($_width) ? 'col-sm' : 'col-sm-' . $_width;
        } else {
            $divWidth = '';

            foreach ($_width as $screenWidth => $width) {
                $divWidth .= ' col-' . $screenWidth . '-' . $width;
            }
        }

        $class = is_null($_class) ? '' : ' ' . $_class;

        if (!is_null($_screenWidth)) {
            $class .= ' d-none d-' . $_screenWidth . '-flex';
        }

        $div = '<div class="' . $divWidth . $class . '">';
        $this->viewport[] = $div;
        $this->reportCell = array();
        $this->reportCellWidth = $_width;
    }

    /**
     *
     * @throws Exception
     */
    public function endGridCell()
    {
        $this->viewport[] = "</div><!-- ./grid_cell -->";
        if (!empty($this->reportCell)) {
            $this->addReportCell($this->reportCell, $this->reportCellWidth);
        }
    }

    /**
     * @param $_input
     * @throws Exception
     */
    public function addNavigation($_input)
    {
        // list
        // label	optional
        // class	optional

        // list:
        // link
        // text
        // active	optional
        // modal	optional

        $label = !isset ($_input["label"]) ?
            '' : ' aria-label="' . $this->getSafe($_input['label']) . '"';

        $class = !isset ($_input['class']) ?
            '' : ' class="' . $_input['class'] . '"';

        $this->viewport[] = "<nav" . $label . $class . '>';

        $this->viewport[] = '<ul class="pagination">';

        foreach ($_input['list'] as $item) {
            $active = !isset ($item['active']) ? '' :
                ($item['active'] ? " active" : '');

            $modal = !isset ($item["modal"]) ? false : $item["modal"];

            $this->viewport[] = '<li class="page-item' . $active . '">';

            $this->addLink(
                $item['link'],
                $item['text'],
                [
                    "class" => "page-link",
                    "modal" => $modal
                ]
            );

            $this->viewport[] = "</li>";
        }

        $this->viewport[] = "</ul>";

        $this->viewport[] = "</nav><!-- ./navigation -->";
    }

    /**
     * Add Tabs
     * @param $_input
     * @throws Exception
     */
    public function addTabs($_input)
    {
        // list
        //      link
        //      text
        //      active (optional)
        $class = $_input['class'] ?? null;
        $this->startTabs($class);
        foreach ($_input['list'] as $item) {
            $this->addTab(
                $item['text'],
                $item['link'],
                $item['active'] ?? false
            );
        }
        $this->endTabs();
    }

    private function startTabs(?string $_class): void {
        $class = $_class ? ' ' . $_class : '';
        $this->viewport[] = '<ul class="nav nav-tabs mt-2' . $class . '">';
    }

    /**
     * @throws Exception
     */
    private function addTab(
        string $_text,
        string $_link,
        ?bool $_isActive
    ): void {
        $this->viewport[] = '<li class="nav-item">';
        $active = ($_isActive ?? false) ? ' active' : '';
        $this->addLink(
            $_link,
            $_text,
            ['class' => 'nav-link' . $active]
        );
        $this->viewport[] = "</li>";
    }

    private function endTabs(): void {
        $this->viewport[] = "</ul>";
    }

    /**
     * @throws Exception
     */
    public function addTabsFromArray(
        array $_tabs,
        string $_link,
        string $_parameter,
        ?string $_activeTab = null
    ): void {
        $this->startTabs(null);
        foreach ($_tabs as $tab) {
            $this->addTab(
                $tab,
                $_link.'&'.$_parameter.'='.$tab,
                ($tab == $_activeTab)
            );
        }
        $this->endTabs();
    }

    /**
     *
     */
    public function startClearfix()
    {
        $this->viewport[] = '<div class="clearfix">';
    }

    /**
     * End Clearfix
     */
    public function endClearfix()
    {
        $this->viewport[] = '</div><!-- ./clearfix -->';
    }

    /**
     * @param $_left
     * @param $_right
     * @param array|null $_left_input
     * @param array|null $_right_input
     * @param string|null $_align
     * @throws Exception
     */
    public function addPair(
        $_left,
        $_right,
        ?array $_left_input = null,
        ?array $_right_input = null,
        ?string $_align = null
    ): void
    {
        $this->startPair($_align);
        $this->addDiv($_left, $_left_input);
        $this->addDiv($_right, $_right_input);
        $this->endPair();
    }

    /**
     * @param string|null $_align
     */
    public function startPair(?string $_align = null)
    {
        $align = $_align ?? 'start';

        $this->startDiv(
            [
                'class' =>
                    'd-flex justify-content-between align-items-' . $align
            ]
        );
    }

    /**
     *
     */
    public function endPair(): void
    {
        $this->viewport[] = "</div><!-- ./pair -->";
    }

    /**
     * @param string $_id
     * @throws Exception
     */
    public function startModal(string $_id)
    {
        if (isset ($this->modal_id)) {
            throw new Exception (
                "Modal '" . $this->modal_id . "' needs to be " .
                "ended before starting a new one"
            );
        }

        $this->modal_id = $_id;

        $this->startDiv
        (
            [
                "class" => "modal",
                "id" => $this->modal_id
            ]
        );

        $this->startDiv(['class' => 'modal-dialog']);
        $this->startDiv(['class' => 'modal-content']);
    }

    /**
     * End Modal
     */
    public function endModal(): void
    {
        $this->viewport[] = '</div><!-- ./modal-content -->';
        $this->viewport[] = '</div><!-- ./modal-dialog -->';
        $this->viewport[] = '</div><!-- ./modal -->';

        unset ($this->modal_id);
    }

    /**
     * @param $_text
     * @param bool $_close
     * @throws Exception
     */
    public function addModalTitle(
        $_text,
        ?bool $_close = true
    )
    {
        $this->startDiv(['class' => "modal-header"]);

        $this->addH2($_text, ['class' => "modal-title"]);

        if ($_close) {
            $this->viewport[] = "<button class=\"close\" " .
                "data-dismiss=\"modal\">&times;" .
                "</button>";
        }

        $this->viewport[] = "</div><!-- ./modal-header -->";
    }

    /**
     *
     */
    public function startModalBody()
    {
        $this->startDiv(['class' => "modal-body"]);
    }

    /**
     *
     */
    public function endModalBody()
    {
        $this->viewport[] = "</div><!-- ./modal-body -->";
    }

    /**
     *
     */
    public function startModalFooter()
    {
        $this->startDiv(['class' => "modal-footer"]);
    }

    /**
     *
     */
    public function endModalFooter()
    {
        $this->viewport[] = "</div><!-- ./modal-footer -->";
    }

    /**
     * @param string|null $_text
     * @return array
     */
    public function code(?string $_text): array
    {
        return ([self::START_CODE, $_text, self::END_CODE]);
    }

    /**
     * @param string|null $_string
     * @return string[]
     */
    public function strikethrough(?string $_string): array {
        return [self::START_STRIKETHROUGH, $_string, self::END_STRIKETHROUGH];
    }

    /**
     * @param $_text
     * @return string|array|null
     * @throws Exception
     */
    public function getSafe($_text)
    {
        if (!is_array($_text)) {
            $safe_string = $this->getXssProtected($_text);
        } else {
            $safe_string = $this->getUnpackedSafe($_text);
        }

        return $safe_string;
    }

    /**
     * @param array $_text
     * @return string|array|null
     * @throws Exception
     */
    public function getUnpackedSafe(array $_text)
    {
        $safeString = '';

        if (count($_text) == 2 and is_bool($_text[0])) {
            if ($_text[0]) {
                $safeString = $_text[1];
            } else {
                $safeString = $this->getXssProtected($_text [1]);
            }
        } else {
            foreach ($_text as $substring) {
                if (!empty($substring)) {
                    if (is_array($substring)) {
                        if ($substring [0]) {
                            $safeString .= $substring [1];
                        } else {
                            $safeString .= $this->getXssProtected($substring [1]);
                        }
                    } else {
                        if (is_bool($substring)) {
                            throw new Exception ("Safe string should be in an array.");
                        }

                        $safeString .= $this->getXssProtected($substring);
                    }
                }
            }
        }

        return $safeString;
    }

    /**
     * @param string|null $_text
     * @return string|null
     */
    private function getXssProtected(?string $_text): ?string
    {
        if ($this->isInXssWhitelist($_text) or is_null($_text)) {
            return $_text;
        } else {
            return htmlspecialchars($_text);
        }
    }

    /**
     * @param $_text
     * @return bool
     */
    private function isInXssWhitelist($_text): bool
    {
        return in_array($_text, self::WHITELIST);
    }


    /**
     * @param $_text
     * @return array
     */
    public function deemSafe($_text): array
    {
        return [true, $_text];
    }

    /**
     * @param $_name
     */
    public function addAnchor($_name)
    {
        $this->viewport[] = "<a id=\"" . $_name . "\"></a>" .
            "<!-- " . $_name . " anchor -->";
    }

    /**
     * @param $_url
     */
    public function addQrCode($_url)
    {
        $this->viewport[] = "<img src=\"" .
            "https://chart.googleapis.com/chart" .
            "?chs=300x300" .
            "&amp;cht=qr" .
            "&amp;chld=" . self::QR_CODE_ERROR_CORRECTION_LEVEL .
            "&amp;chl=" . urlencode($_url) .
            "&amp;choe=UTF-8\" " .
            "alt=\"QR code\" /><br>";
    }

    /**
     *
     */
    public function startJumbotron()
    {
        $this->startDiv(['class' => "jumbotron"]);
    }

    /**
     *
     */
    public function endJumbotron()
    {
        $this->viewport[] = "</div><!-- ./jumbotron -->";
    }

    /**
     * Clear
     * Clears the content of this viewport
     */
    public function clear()
    {
        $this->viewport = array();
    }

    /**
     * Add Pagination Navigation
     * @param array $_list
     * @throws Exception
     */
    public function addPaginationNavigation(array $_list)
    {
        $this->addNavigation(
            [
                'label' => 'Page navigation',
                'list' => $_list
            ]
        );
    }

    /**
     * @return array
     */
    public function getViewportData(): array
    {
        return $this->viewport;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getContent(): string
    {
        $content = '';
        foreach ($this->viewport as $line) {
            $content .= is_array($line) ? $this->getUnpackedSafe($line) : $line . "\n";
        }

        return $content;
    }

    /**
     * @throws Exception
     */
    public function display()
    {
        echo $this->getContent();
    }

    /**
     * @throws Exception
     */
    public function output()
    {
        if (!isset ($this->header)) {
            // CSV file

            $this->outputCsv();
        } else {
            // Something else, e.g. calendar

            $this->display();
        }
    }

    /**
     *
     */
    public function outputCsv()
    {
        // From http://php.net/manual/en/function.fputcsv.php

        $outStream = fopen("php://memory", 'r+');

        foreach ($this->report as $row) {
            fputcsv($outStream, $row, "\t");
        }

        rewind($outStream);

        $output = stream_get_contents($outStream);

        fclose($outStream);

        // From https://stackoverflow.com/a/4440143/2511355

        //echo "\xEF\xBB\xBF"; // UTF-8 BOM

        echo "\xFF\xFE"; // UTF-16LE BOM

        echo mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');
    }

    /**
     * Add Image From File
     * From https://stackoverflow.com/a/21600709/2511355
     * Serves an image from a location that is not accessible by the browser.
     * @param $_path
     */
    public function addImageFromFile(
        string $_path,
        ?string $_altText = null,
        ?array $_input = null
    ): void {
        $src = $this->getImageSourceFromFile($_path);
        $this->addImage(
            $src,
            $_altText,
            $_input
        );
    }

    /**
     * Get Image Source From File
     * Based on https://stackoverflow.com/a/21600709/2511355
     * Format the image SRC:  data:{mime};base64,{data};
     * @param string $_path
     * @return string
     */
    protected function getImageSourceFromFile(
        string $_path
    ): string {
        // Read image path, convert to base64 encoding
        $imageData = base64_encode(file_get_contents($_path));
        $type = mime_content_type($_path);
        $src = 'data: ' . $type . ';base64,' . $imageData;

        return $src;
    }

    /**
     * Add Image
     * Serves an image from a location that is accessible by the browser.
     * @param string $_source
     * @param string|null $_alt
     * @param array|null $_input
     */
    public function addImage(
        string $_source,
        ?string $_alt = '',
        ?array $_input = array()
    ): void
    {
        if (isset($_input['class'])) {
            $class = $_input['class'];
        } else {
            $class = '';
        }

        if ($_input['fluid'] ?? true) {
            $class .= (empty($class) ? '' : ' ') . 'img-fluid';
        }
        $image = '<img src="' . $_source . '" alt="' . $_alt. '"';
        $image .= ' class="'. $class .'">';
        if (isset($_input['link'])) {
            $this->viewport[] = '<a href="'.$_input['link'].'">'.$image.'</a>';
        } else {
            $this->viewport[] = $image;
        }
    }

    public function addShortcutIconFromFile(string $_filename)
    {
        $iconData = base64_encode(file_get_contents($_filename));
        $src = 'data: ' . mime_content_type($_filename) . ';base64,' . $iconData;
        $this->viewport[] = '<link href=\'' . $src . '\' rel="icon" type="image/x-icon" />';
    }

    public function getConvertedToId($_label): string
    {
        // Converts string to lower case and strips out whitespace
        return strtolower($this->getConvertedWhitespaceToUnderscore($_label));
    }

    /**
     * Get Converted Whitespace To Underscore
     * From https://stackoverflow.com/a/40048457/2511355
     * @param $string
     * @return array|string|null
     */
    private function getConvertedWhitespaceToUnderscore($string)
    {
	return preg_replace(
    "/(\t|\n|\v|\f|\r| |\xC2\x85|\xc2\xa0|\xe1\xa0\x8e|\xe2\x80[\x80-\x8D]|\xe2\x80\xa8|\xe2\x80\xa9|\xe2\x80\xaF|\xe2\x81\x9f|\xe2\x81\xa0|\xe3\x80\x80|\xef\xbb\xbf)+/",
		"_",
		$string);
    }

    public function startPage(
        string $title,
        ?string $shortcutIconPath = null,
        bool $isOffline = false,
        ?DebugBar $debugBar = null,
        ?string $auxiliaryHeaderScript = null
    ): void {
        $this->startHtml();
        $this->addHead(
            $title,
            $shortcutIconPath,
            $isOffline,
            $debugBar,
            $auxiliaryHeaderScript
        );
        $this->startBody();
    }

    public function endPage(): void {
        $this->endBody();
    }

    public function startHtml(): void
    {
        $this->viewport[] = '<!DOCTYPE html>';
        $this->viewport[] = '<html lang="' . self::HTML_LANGUAGE . '">';
    }

    public function endHtml()
    {
        $this->viewport[] = '</html>';
    }

    public function addHead(
        string $title,
        ?string $shortcutIconPath = null,
        bool $isOffline = false,
        ?DebugBar $debugBar = null,
        ?string $auxiliaryHeaderScript = null
    ): void {
        $this->startHtml();
        $this->viewport[] = '<HEAD>';
        $this->viewport[] = '<meta charset="utf-8">';
        $this->viewport[] = '<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">';
        $this->viewport [] = <<<HEREDOC
HEREDOC;
        if ($isOffline) {
            $this->viewport[] = <<<HEREDOC
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/jquery-ui.css">
HEREDOC;
        } else {
            $this->viewport[] = <<<HEREDOC
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
HEREDOC;
        }
        if ($auxiliaryHeaderScript) {
            $this->viewport[] = $auxiliaryHeaderScript;
        }
        $this->viewport[] = <<<HEREDOC
HEREDOC;
        $this->viewport [] = '<title>' . $title . '</title>' . PHP_EOL;

        if ($shortcutIconPath) {
            $this->addShortcutIconFromFile($shortcutIconPath);
        }

        $this->viewport [] = <<<HEREDOC
<STYLE>
@media (max-width: 768px) {
	html {
    font-size: 0.8rem;
  }
}
</STYLE>

HEREDOC;

        $this->endHead($debugBar);
    }

    /**
     * @param DebugBar|null $_debugBar
     */
    public function endHead(?DebugBar $_debugBar = null)
    {
        if (!is_null($_debugBar) and !is_null($_debugBar->getDebugBar())) {
            $debugBarRenderer = $_debugBar->getDebugBar()->getJavascriptRenderer();
            $debugBarRenderer->setBaseUrl('debugbar');
            $this->viewport[] = $debugBarRenderer->renderHead();
        }
        $this->viewport[] = '</HEAD>';
    }

    public function startBody(?bool $includeContainer = true)
    {
        $this->viewport[] = '<BODY>';
        if ($includeContainer) {
            $this->startDiv(['class' => 'container']);
        }
    }

    /**
     * @param Jquery|null $_jQuery
     * @param DebugBar|null $_debugBar StandardDebugBar
     * @param bool|null $_useLocal
     * @param bool|null $_enableBootstrapTooltips
     */
    public function endBody(
        Jquery $_jQuery = null,
        ?DebugBar $_debugBar = null,
        ?bool $_useLocal = false,
        ?bool $_enableBootstrapTooltips = true
    ): void {
        if ($_useLocal) {

            $this->viewport[] = /** @lang html */
                <<<HEREDOC
<script
			  src="js/jquery-3.4.1.min.js"></script>
<script
			  src="js/jquery-ui.min.js"></script>
<script src="js/popper.min.js" ></script>
<script src="js/bootstrap.min.js"></script>

HEREDOC;

        } else {

            $this->viewport[] = /** @lang html */
                <<<HEREDOC
<script     src="https://code.jquery.com/jquery-3.5.1.min.js"
            integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
            crossorigin="anonymous"></script>
			  
<script     src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct"
            crossorigin="anonymous"></script>

<script     src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
            integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
            crossorigin="anonymous"></script>

HEREDOC;
        }
        if (isset ($this->auxiliaryHeaderScripts)) {
            $this->viewport[] = $this->auxiliaryHeaderScripts;
        }
        if ($_jQuery and $_jQuery->exists()) {
            $this->viewport[] = $_jQuery->getContent();
        }
        if ($_enableBootstrapTooltips) {
            $this->viewport[] =
                <<<HEREDOC
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
HEREDOC;
        }
        if (!is_null($_debugBar) and !is_null($_debugBar->getDebugBar())) {
            $debugBarRenderer = $_debugBar->getDebugBar()->getJavascriptRenderer();
            $this->viewport[] = $debugBarRenderer->render();
        }
        $this->viewport[] = '</BODY>';
        $this->endHtml();
    }

    /**
     * @throws Exception
     */
    public function addProgressBar(): void {
        $this->addDiv('', ['id' => 'progressbar']);
    }

    /**
     * Add Grid Header
     * E.g.
     * [
     * 'Period'            => ['width' => 1],
     * 'From date'         => ['width' => 2],
     * 'Opening balance'   => ['width' => 2, 'class' => 'text-right']
     * ]
     * @param array $_columnHeadings
     * @param string|null $_class
     * @throws Exception
     */
    public function addGridHeader(
        array $_columnHeadings,
        ?string $_class = null
    ): void {
        if ($_class) {
            $class = ' '.$_class;
        } else {
            $class = '';
        }
        $this->startGridRow("lead bg-primary text-light pt-2 pb-2 align-items-end".$class);
        foreach ($_columnHeadings as $headingText => $headingValues) {
            $this->startGridCell($headingValues['width'] ?? null,
                $headingValues['class'] ?? null);
            if ($headingValues['tooltip'] ?? false) {
                $this->startToolTip($headingValues['tooltip']);
            }
            $this->addDiv($headingText);
            if ($headingValues['tooltip'] ?? false) {
                $this->endTooltip();
            }
            $this->endGridCell();
        }
        $this->endGridRow();
    }

    /**
     * @param array $rows
     * @throws Exception
     */
    public function addRowCount(array $rows): void
    {
        $count = count($rows);
        $this->addParagraph(
            $count . ' ' .
            $count == 1 ? 'row' : 'rows'
        );
    }

    public function startNavBar(): void
    {
        $this->viewport[] = '<div class="sticky-top">';
    }

    public function endNavBar(): void
    {
        $this->viewport[] = '</div>';
    }

    public function startNavBarRow(string $_class): void
    {
        $this->viewport[] = '<nav class="navbar navbar-expand-lg ' . $_class . '">';
    }

    public function endNavBarRow(): void
    {
        $this->viewport[] = '</nav>';
    }

    public function addNavBarBrand(
        string $_label,
        ?string $_imageSource = null
    ): void {
        $this->viewport[] = '<div class="container">';
        $this->viewport[] = '<a class="navbar-brand" href="' . $_SERVER['SCRIPT_NAME'] . '">';
        if ($_imageSource) {
            $this->addImage(
                $_imageSource,
                '',
                ['class' => 'd-inline-block align-top']
            );
        }
        $this->viewport[] = $_label . '</a>';
        $this->navbarBrandIsOpen = true;
    }

    public function startNavBarItems(): void {
        $navbarTogglerId = 'navbarToggler' . $this->navbarId;
        $this->viewport[] = '<button class="navbar-toggler" type="button" data-toggle="collapse" ' .
            'data-target="#' . $navbarTogglerId . '" aria-controls="' . $navbarTogglerId . '" ' .
            'aria-expanded="false" aria-label="Toggle navigation">';
        $this->viewport[] = '<span class="navbar-toggler-icon"></span>';
        $this->viewport[] = '</button>';
        if ($this->navbarBrandIsOpen) {
            $this->viewport[] = '</div>';
            $this->navbarBrandIsOpen = false;
        }
        $this->viewport[] = '<div class="collapse navbar-collapse" id="' . $navbarTogglerId  . '">';
        $this->viewport[] = '<ul class="navbar-nav pl-2">';
        $this->navbarId++;
    }

    public function endNavBarItems(): void {
        $this->viewport[] = '</ul>';
        $this->viewport[] = '</div>';
    }

    public function addNavBarItem(
        string $_link,
        string $_label
    ): void
    {
        $this->viewport[] = '<li class="nav-item">';
        $this->viewport[] = '<a class="nav-link" href="' . $_link . '">' . $_label. '</a>';
        $this->viewport[] = '</li>';
    }

    public function startNavBarDropdown(
        string $_label
    ): void
    {
        $this->viewport[] = '<li class="nav-item dropdown">';
        $this->viewport[] = '<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-expanded="false">' . $_label . '</a>';
        $this->viewport[] = '<div class="dropdown-menu">';
    }

    public function endNavBarDropdown(): void
    {
        $this->viewport[] = '</div>';
        $this->viewport[] = '</li>';
    }

    public function addNavBarDropdownItem(
        string $_link,
        string $_label
    ): void
    {
        $this->viewport[] = '<div class="menu">' . PHP_EOL. '<span class="menuitemright">';
        $this->viewport[] = '<a href="' . $_link . '" class="dropdown-item">' . $_label . '</a>';
        $this->viewport[] = '</span>' . PHP_EOL. '</div>';
    }

    /**
     * @throws Exception
     */
    public function addCarousel(
        array $_content,
        ?string $_id = 'carousel'
    ): void
    {
        $active = 0;
        $this->viewport[] = '<div id="'.$_id.'" class="carousel slide" data-ride="carousel">';
        $this->viewport[] = '<ol class="carousel-indicators">';
        for ($i = 0; $i < count($_content); $i++) {
            $indicator = '<li data-target="#'.$_id.'" data-slide-to="' . $i . '"';

            if ($i == $active) {
                $indicator .= ' class="active"';
            }
            $indicator .= '></li>';
            $this->viewport[] = $indicator;
        }
        $this->viewport[] = '</ol>';

        $this->viewport[] = '<div class="carousel-inner">';
        foreach ($_content as $seq => $inner) {
            $link = $inner['link'] ?? null;
            $item = '<div class="carousel-item';
            if ($seq == $active) {
                $item .= ' active';
            }
            $item .= '">';
            $this->viewport[] = $item;
            if ($link) {
                $this->startLink($link, 'text-decoration-none');
            }
            $this->viewport[] ='<img src="'.$inner['image'].'" class="d-block w-100" alt="...">';
            $this->viewport[] = '<div class="carousel-caption d-none d-block">';

            if (isset($inner['label'])) {
                // Show only on md lg xl
                $this->viewport[] = '<h1 class="text-white d-none d-md-block">'.$inner['label'].'</h1>';
                // Show only on xs sm
                $this->viewport[] = '<h2 class="text-white d-md-none">'.$inner['label'].'</h2>';
            }
            if (isset($inner['content'])) {
                // Show only on md lg xl
                $this->viewport[] = '<h5 class="text-white d-none d-md-block">'  . $inner['content'] . '</h5>';
                // Show only on xs sm
                $this->viewport[] = '<p class="text-white d-md-none">'  . $inner['content'] . '</p>';
            }
            if ($link) {
                $this->endLink();
            }
            if (isset($inner['button'])) {
                if ($link) {
                    $this->startLink($link);
                }
                $this->addButton($inner['button']);
                if ($link) {
                    $this->endLink();
                }
            }
            $this->viewport[] = '</div>';
            $this->viewport[] = '</div>';
        }
        $this->viewport[] = '</div>';
        $this->viewport[] = '<a class="carousel-control-prev" href="#'.$_id.'" role="button" data-slide="prev">';
        $this->viewport[] = '<span class="carousel-control-prev-icon" aria-hidden="true"></span>';
        $this->viewport[] = '<span class="sr-only">Previous</span>';
        $this->viewport[] = '</a>';
        $this->viewport[] = '<a class="carousel-control-next" href="#'.$_id.'" role="button" data-slide="next">';
        $this->viewport[] = '<span class="carousel-control-next-icon" aria-hidden="true"></span>';
        $this->viewport[] = '<span class="sr-only">Next</span>';
        $this->viewport[] = '</a>';
        $this->viewport[] = '</div><!-- ./carousel -->';
    }

    public function startList(): void
    {
        $this->viewport[] = '<ul>';
    }

    public function endList(): void
    {
        $this->viewport[] = '</ul>';
    }

    public function addBullet(string $_text): void
    {
        $this->viewport[] = '<li>' . $_text . '</li>';
    }

    /**
     * Add Navbar Search
     * Adds a search field in the navbar
     * @param string $destination
     * @param string $searchParameter
     * @param string $id
     * @param string $placeholder
     * @param string $scriptName
     * @param string $parameterName
     * @return void
     */
    public function addNavbarSearch(
        string $destination,
        string $searchParameter,
        string $id,
        string $placeholder,
        string $scriptName,
        string $parameterName
    ): void {
        $this->viewport[] = '<form class="form-inline my-lg-0 ml-auto" METHOD="get" ACTION="'. $scriptName.'" NAME="query">';
        $this->viewport[] = '<INPUT TYPE="hidden" NAME="' . $parameterName . '" VALUE="' . $destination .  '">';
        $this->viewport[] = '<input type="text" class="form-control mr-sm-2" name="' . $searchParameter . '"' .
            ' id="' . $id . '" placeholder="'.$placeholder.'">';
        $this->viewport[] = '<input type="submit" class="btn btn-primary my-2 my-sm-0" value="Find">';
        $this->viewport[] = '</form>';
    }

    /**
     * @throws Exception
     */
    public function addReportTitleAndDownloadButton(
        string $_title,
        string $_link
    ): void {
        $this->startGridRow();
        $this->startGridCell();
        $this->addH1($_title);
        $this->endGridCell();
        $this->startGridCell(2, 'align-self-center text-right');
        $this->addDownloadButton($_link);
        $this->endGridCell();
        $this->endGridRow();
    }

    /**
     * @throws Exception
     */
    private function addDownloadButton(string $_link): void
    {
        $this->startGridCell(null, "align-self-center");
        $this->addButton(
            [
                "label" => $this->deemSafe(self::ICON_DOWNLOAD . " Download"),
                "small" => true,
                "link"  => $_link
            ]
        );
        $this->endGridCell();
    }

    /**
     * @throws Exception
     */
    public function addLead(
        $_text,
        $_input = array()
    ): void {
        if (is_string($_input)) {
            $input = ['class' => $_input];
        } else {
            $input = $_input;
        }
        $class = 'lead';
        if ($input['class'] ?? false) {
            $input['class'] .= ' ' . $class;
        } else {
            $input['class'] = $class;
        }

        $this->addDiv($_text, $input);
    }

    /**
     * Start
     * This is the screen version of the ViewportEmail start() method
     * @throws Exception
     */
    public function start(string $_text): void {
        $this->addLead($_text, ['class' => 'text-muted']);
    }

    public function startTooltip(string $_tooltipText): void {
        $this->viewport[] = '<span data-toggle="tooltip" data-placement="top" title="' . $_tooltipText . '">';
    }

    public function endTooltip(): void {
        $this->endSpan();
    }

    public function startContainer(
        ?string $_class = null,
        ?string $_style = null
    ): void {
        $style = !$_style ? '' : 'style="' . $_style . '"';
        $this->viewport[] = '<div class="container' . (!$_class ? '' : ' '.$_class) . '"' . $style . '>';
    }

    public function endContainer(
        ?string $_type = null
    ): void {
        $this->viewport[] = '</div><!-- ./' . ($_type ?? '') . ' -->';
    }
}