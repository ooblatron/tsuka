<?php

namespace Tsuka\Traits;

use Exception;
use Tsuka\FormErrorManager;
use Tsuka\Interfaces\Session;
use function array_pop;
use function bin2hex;
use function count;
use function get_called_class;
use function hash_hmac;
use function headers_sent;
use function is_array;
use function is_numeric;
use function random_bytes;
use function reset;
use function session_start;
use function session_status;
use function session_write_close;
use const PHP_SESSION_ACTIVE;

trait BootstrapForm
{
    /**
     * @var FormErrorManager $formErrorManager
     */
    protected $formErrorManager;

    /**
     * @throws Exception
     */
    function setSaveDestination(
        ?string $_destination = null,
        ?array $_input = null
    ): void {
        $type = $_input['type'] ?? self::POST;
        $upload = $_input['upload'] ?? false;
        $inline = $_input['inline'] ?? false;

        if (isset ($_destination)) {
            $this->viewport[] = $this->getFormTag($type, $_destination, $upload, $inline);
            if (isset($this->params)) {
                $this->addAntiCsrfFields();
            }
        }
    }

    /**
     * @param string $_type
     * @param string $_destination
     * @param bool $_upload
     * @param bool $_inline
     * @return string
     */
    function getFormTag(
        string $_type,
        string $_destination,
        bool $_upload,
        bool $_inline
    ): string {
        $form_tag = '<form METHOD="' . $_type . '" ACTION="' . $_destination . '"';

        if ($_upload) {
            $form_tag .= ' enctype="multipart/form-data"';
        }

        if ($_inline) {
            $form_tag .= ' class="form-inline"';
        }

        $form_tag .= '>';

        return $form_tag;
    }

    /**
     * @throws Exception
     */
    protected function addAntiCsrfFields(): void
    {
        $this->addInputHidden([
            'name'  => self::ANTI_CSRF_TOKEN_PARAMETER,
            'value' => $this->getAntiCsrfToken($this->getFormName())
        ]);
        $this->addInputHidden([
            'name'  => self::FORM_NAME_PARAMETER,
            'value' => $this->getFormName()
        ]);
    }

    protected function getFormName(): string
    {
        return get_called_class();
    }

    /**
     * @param $_input
     */
    function addInputHidden($_input): void {

        // Inputs
        //
        // name
        // value
        // id		optional

        $id = "";

        if (isset ($_input['id']))
            $id = ' id="'.$_input['id'].'"';

        $this->viewport[] = '<INPUT TYPE="hidden"'.
            $id.
            ' NAME="'.$_input['name'].'"'.
            ' VALUE="'.$_input['value'].'">';

    }

    /**
     * @param array $_input
     * @param bool $_isRow
     * @throws Exception
     */
    function addInputText(
        array $_input,
        bool $_isRow = false
    ): void {
        // Inputs
        //
        // placeholder  optional
        // readonly     optional
        // type         optional
        // name         optional
        // value        optional
        // type		    optional
        // label	    optional
        // id		    optional
        // note		    optional
        // class	    optional

        # class defaults to empty string
        $class = !isset ($_input['class']) ? '' : " ".$_input['class'];

        $this->viewport[] = '<div class="form-group'.($_isRow ? ' row' : '').$class.'">';

        $this->addInlineInputText($_input, $_isRow); // See viewport class

        if (isset ($_input['note'])) {
            if ($_input['note'] != '') {
                $this->viewport[] =
                    '<small class="form-text text-muted">' .
                    $this->getSafe($_input['note']) . '</small>';
            }
        }

        $this->viewport[] = '</div>';
    }

    /**
     * @param $_input
     * @throws Exception
     */
    public function addInputTextRow($_input): void
    {
        // Inputs
        //
        // type		optional
        // label	optional
        // id		optional
        // note		optional
        // class	optional

        $this->addInputText($_input, true);
    }

    /**
     * @param string $_name
     */
    function addInputMonth(string $_name): void
    {
        $this->viewport[] = '<div class="form-group">';
        $this->viewport[] = '<label for="monthPicker">Month</label>';
        $this->viewport[] = '<input'.
            ' type="month"'.
            ' id="monthPicker"'.
            ' name="'.$_name.'"'.
            '>';
    }

    /**
     * @param array $_input
     * @throws Exception
     */
    function addDropdown(array $_input): void
    {
        // label	optional
        // name
        // list
        // value
        // id		optional
        // class	optional
        // nullable	optional
        // readonly optional

        # class defaults to empty string
        $class = !isset ($_input['class']) ? '' : " ".$_input['class'];

        $this->viewport[] = '<div class="form-group'.$class.'">';

        $this->addInlineDropdown($_input);

        $this->viewport[] = '</div>';

    }

    /**
     * @param array $_input
     * @throws Exception
     */
    function addInlineDropdown(array $_input): void
    {
        // name
        // list
        // value
        // label	optional
        // id		optional
        // nullable	optional
        // cols		optional
        // class	optional
        // readonly optional

        # id defaults to label or name
        $id = !isset($_input['id']) ? $this->getConvertedToId(
            $_input['label'] ?? $_input['name']) : $_input['id'];

        # nullable defaults to TRUE
        $nullable = !isset ($_input['nullable']) || $_input['nullable'];

        $blank_exists = FALSE;

        $class = "form-control";

        if (isset ($_input['label']))
            $this->viewport[] = '<label for="'.$id.'">'.
                $this->getSafe($_input['label']).
                '</label>';

        if (isset ($_input['cols']))
            $class .= " col-sm-".$_input['cols'];

        if (isset ($_input['class']))
            $class .= " ".$_input['class'];

        $this->viewport[] = '<SELECT NAME="'.$_input['name'].'"'.
            ' class="'.$class.'"'.
            ' id="'.$id.'">';

        $list = '';

        foreach ($_input['list'] as $listKey => $listValue) {

            if (is_array($listValue)) {
                $key = reset($listValue);
                $value = array_pop($listValue);
            } else {
                $key = $listKey;
                $value = $listValue;
            }

            $list .= '<OPTION VALUE="'.$key.'"';

            if ($_input['value'] == $key){

                $list .= ' SELECTED';

            } else {

                if ($_input['readonly'] ?? false)
                    $list .= " DISABLED";

            }

            $list .= '>'.$value."</OPTION>\n";

            if ($value == '')
                $blank_exists = TRUE;

        }

        // Add blank dropdown option if one was not provided in passed list
        // and value can be null

        if (!$blank_exists and $nullable)
            $list = $this->addBlankDropdownOption($_input['value']).$list;

        $list .= '</SELECT>';

        $this->viewport[] = $list;

    }

    /**
     * @param string|null $_value
     * @return string
     */
    function addBlankDropdownOption(?string $_value): string
    {
        $option = '<OPTION label=" "';
        if ($_value === '') {
            $option .= ' SELECTED';
        }
        $option .= '></OPTION>' . "\n";

        return $option;
    }

    /**
     * @param array $_input
     * @throws Exception
     */
    function startInline(array $_input): void {

        # Bottom margin defaults to 5
        $mb = !isset($_input['mb']) ? 5 : $_input['mb'];

        $this->viewport[] = '<div class="form-inline mb-'.$mb.'">';

        if (isset($_input['legend']))
            $this->viewport[] = '<legend>'.$this->getSafe($_input['legend']).'</legend>';

    }

    /**
     *
     */
    function endInline(): void
    {
        $this->viewport[] = '</div>';
    }

    /**
     * @param array $_input
     * @throws Exception
     */
    function addRadioButtons(array $_input): void
    {

        //	label
        //	name
        //	value
        //	list
        //	class	optional


        # class defaults to mb-3

        if (!(isset ($_input['class']))) {
            $this->viewport[] = '<div class="mb-3">';
        } else {
            $this->viewport[] = '<div class="'.$_input['class'].'">';
        }

        $this->viewport[] = '<fieldset class="form-group-md">';

        if (isset ($_input['label']))
            $this->viewport[] = '<legend>'.$this->getSafe($_input['label'])."</legend>";

        $list = $_input['list'];
        if (is_array($list)) {
            // list is an array
            foreach ($list as $row) {
                $this->addRadioButtonItem(
                    $_input['name'],
                    $row[0],
                    $row[1],
                    $_input['value']
                );
            }
        } else {
            // list is a collection
            foreach ($list as $key => $value) {
                $this->addRadioButtonItem(
                    $_input['name'],
                    $key,
                    $value,
                    $_input['value']
                );
            }
        }

        $this->viewport[] = '</fieldset>';
        $this->viewport[] = '</div>';

    }

    private function addRadioButtonItem(
        string $_name,
        string $_key,
        string $_value,
        ?string $_selectedKey
    ): void {
        $this->viewport[] = '<div class="form-check">'. "\n".
            '<label class="form-check-label">';

        $list = '<INPUT TYPE="RADIO" '.
            'NAME="'.$_name.'" '.
            'class="form-check-input" '.
            'VALUE="'.$_key.'"';

        if ($_selectedKey == $_key) $list .= self::CHECKED;

        $list .= '>'.$_value;
        $this->viewport[] = $list;
        $this->viewport[] = '</label>';
        $this->viewport[] = '</div>';
    }

    /**
     * @param array $_input
     * @throws Exception
     */
    function addTextArea(array $_input): void
    {
        # id defaults to label
        $id = !isset($_input['id']) ?
            $this->getConvertedToId($this->getSafe($_input['label'])) :
            $_input['id'];

        # number of rows defaults to 1
        $rows = !isset($_input['rows']) ? 1 : $_input['rows'];

        $this->viewport[] = '<div class="form-group">';
        $this->viewport[] = '<label for="'.$id.'">'.$this->getSafe($_input['label']).'</label>';
        $this->viewport[] = '<textarea name="'.$_input['name'].'"'.
            ' rows="'.$rows.'"'.
            ' class="form-control"'.
            ' id="'.$id.'">';
        $this->viewport[] = $_input['value'];
        $this->viewport[] = '</textarea>';
        $this->viewport[] = '</div>';
    }

    /**
     * @param array $_input
     * @throws Exception
     */
    function addCheckbox(array $_input): void
    {
        // name
        // value
        // label
        // value_if_checked		optional
        // disabled				optional
        // class				optional
        // id					optional

        # value_if_checked defaults to Y
        $value_if_checked = !isset($_input['value_if_checked']) ? 'Y'
            : $_input['value_if_checked'];

        # disabled defaults to FALSE
        $disabled = !isset($_input['disabled']) ? FALSE : $_input['disabled'];

        # class defaults to mb-3
        $class = !isset ($_input['class']) ? 'class="mb-3"' :
            ($_input['class'] == '' ? '' : " ".$_input['class']);

        $labelClass = $disabled ? ' text-muted' : '';

        # id defaults to empty string
        $id = !isset ($_input['id']) ? '' : ' id="'.$_input['id'].'"';

        $this->viewport[] = '<div '.$class.'>';
        $this->viewport[] = '<div class="form-check">';
        $this->viewport[] = '<label class="form-check-label' . $labelClass . '">';
        $this->viewport[] = '<input type="checkbox"'.
            ' name='.$_input['name'].
            ' value='.$value_if_checked.
            ($_input['value'] ==
            $value_if_checked ? self::CHECKED : '').
            ($disabled ? self::DISABLED : '').
            ' class="form-check-input"'.
            $id.
            '>';
        $this->viewport[] = $this->getSafe($_input['label']);
        $this->viewport[] = '</label>';
        $this->viewport[] = '</div>';
        $this->viewport[] = '</div>';

    }

    /**
     * @param string|array|null $_text
     * @param array|null $_input
     * @param bool|null $_noDiv
     * @throws Exception
     */
    function addSaveButton(
        $_text = '',
        ?array $_input = null,
        ?bool $_noDiv = false
    ): void {
        $input = $_input ?? array();

        // label defaults to Save
        $input['label'] = $_text == '' ? 'Save' : $_text;
        $input['submit'] = true;
        $input['id'] = 'save-button';
        if (!$_noDiv) {
            $this->startDiv();
        }
        $this->addButton($input);
        if (!$_noDiv) {
            $this->endDiv();
        }
    }

    /**
     * @throws Exception
     */
    function display(): void {
        $this->fix();
        parent::display();
    }


    /**
     * @return $this
     */
    function fix()
    {
        $this->viewport[] = '</form>';
        return $this;
    }

    /**
     * Add Error
     * Adds the display of any error messages for the specified parameter to the form
     * @param string $_param
     * @throws Exception
     */
    protected function addError(string $_param): void
    {
        if (isset($this->formErrorManager)) {
            $this->append($this->formErrorManager->showError($_param)->getViewportData());
        }
    }

    /**
     * Inject error
     * Records the specified error message against the specified parameter to be displayed in the form.
     * This method does not actually display the error message: that is done by the addError() method.
     * @param $_param
     * @param $_errorMessage
     * @throws Exception
     */
    public function injectError($_param, $_errorMessage): void
    {
        if (isset($this->formErrorManager)) {
            $this->formErrorManager->addError($_param, $_errorMessage);
        }
    }

    /**
     * @param $_param
     * @return bool|null
     * @throws Exception
     */
    protected function isInvalid($_param): ?bool
    {
        return $this->formErrorManager->isInvalid($_param);
    }

    /**
     * injectFormErrors
     * Injects validation errors into the Form Error Manager. This only works if the child form has a Form Error Manager.
     * @param Session $_userSession
     * @return $this
     */
    public function injectFormErrors(Session $_userSession)
    {
        if ($_userSession->hasFormErrors()) {
            $this->formErrorManager->injectErrors(
                $_userSession->getFormErrors()
            ); // Child form must have formErrorManager
        }
        return $this;
    }

    /**
     * Has Errors
     * Returns whether the child form as errors. This should be called after calling the child form's validate() method.
     * The child form must have a Form Error Manager.
     * @return bool
     */
    public function hasErrors(): bool
    {
        return $this->formErrorManager->hasErrors();
    }

    /**
     * Add Errors
     * Displays all errors. The child form must have a Form Error Manager.
     * @throws Exception
     */
    public function addErrors(): void
    {
        $numErrors = count($this->formErrorManager->errors());
        if ($numErrors) {
            if ($numErrors == 1) {
                $prefix = 'There is a problem';
            } else {
                $prefix = 'There are problems';
            }
            $this->addAlert($prefix.' with the details you have provided.', ['class' => 'alert-danger']);
            foreach ($this->formErrorManager->errors() as $error) {
                $this->addDiv($error, ['class' => 'text-danger ml-4']);
            }
            if ($numErrors == 1) {
                $article = 'this';
            } else {
                $article = 'these';
            }
            $this->addAlert('Please fix '.$article.' and try again.', ['class' => 'alert-danger mt-3']);
        }
    }

    /**
     * Add Save And Back Buttons
     * Adds a grid showing a Save button with a Back button in outline next to it. The back button leads to the passed
     * URL.
     * @throws Exception
     */
    protected function addSaveAndBackButtons(string $_urlBack): void
    {
        $this->addSaveAndOtherButtons(
            $_urlBack,
            'Back'
        );
    }

    /**
     * @throws Exception
     */
    protected function addSaveAndCancelButtons(string $_urlBack): void
    {
        $this->addSaveAndOtherButtons(
            $_urlBack,
            'Cancel'
        );
    }

    /**
     * @throws Exception
     */
    private function addSaveAndOtherButtons(
        string $_urlBack,
        string $_label
    ): void {
        $this->startGrid(null, 'ml-0 pl-0');
        $this->startGridRow('mt-4');
        $this->startGridCell(1);
        $this->addSaveButton();
        $this->endGridCell();
        $this->startGridCell(1);
        $this->addButton([
            'label'     => $_label,
            'link'      => $_urlBack,
            'outline'   => 'true'
        ]);
        $this->endGridCell();
        $this->endGridRow();
        $this->endGrid();
    }

    /**
     * See https://stackoverflow.com/a/31683058/2511355
     * @param string $_formName
     * @return string
     * @throws Exception
     */
    public function getAntiCsrfToken(string $_formName): string
    {
        return hash_hmac(
            self::ANTI_CSRF_HASHING_ALGORITHM,
            $_formName,
            $this->getSavedAntiCsrfToken()
        );
    }

    /**
     * See https://stackoverflow.com/a/31683058/2511355
     * @throws Exception
     */
    public function getSavedAntiCsrfToken(): string
    {
        if (!headers_sent() and session_status() !== PHP_SESSION_ACTIVE) {
            session_start();
        }
        if (isset($_SESSION[self::ANTI_CSRF_TOKEN])) {
            $token = $_SESSION[self::ANTI_CSRF_TOKEN];
        } else {
            $token = bin2hex(random_bytes(32));
            $_SESSION[self::ANTI_CSRF_TOKEN] = $token;
        }
        if (!headers_sent()) {
            session_write_close();
        }

        return $token;
    }
}