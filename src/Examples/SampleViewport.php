<?php

namespace Tsuka\Examples;

use Exception;

class SampleViewport extends Viewport
{
    /**
     * @throws Exception
     */
    public function build(): SampleViewport {
        $this->startPage('Hello world');
        $this->addH1('Hello world');
        $this->endPage();
        return $this;
    }
}