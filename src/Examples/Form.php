<?php

namespace Tsuka\Examples;

use Tsuka\Traits\Bootstrap4Form;

class Form extends Viewport
{
    protected const ANTI_CSRF_TOKEN = 'ANTI_CSRF_SECOND_TOKEN';
    protected const ANTI_CSRF_HASHING_ALGORITHM = 'sha256';
    protected const ANTI_CSRF_TOKEN_PARAMETER = 't0';
    protected const FORM_NAME_PARAMETER = 't1';

    use Bootstrap4Form;
}