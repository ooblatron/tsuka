<?php

namespace Tsuka;

use Exception;
use Tsuka\Interfaces\HtmlEntities;

class ViewportEmail extends Bootstrap implements HtmlEntities
{
    private const LIGHT_COLOUR = '#f7fafc';
    private const SUCCESS_COLOUR = '#198754';
    private const SUCCESS_BG_COLOUR = self::SUCCESS_COLOUR;
    private const SUCCESS_BG_TEXT_COLOUR = self::LIGHT_COLOUR;
    private const LIGHT_BG_COLOUR = self::LIGHT_COLOUR;
    private const SECONDARY_COLOUR = '#e2e8f0';
    private const FONT_WEIGHT_BOLD = 'font-weight: 700';

    /**
     * @var string
     */
    private $bgColor;
    /**
     * @var string
     */
    private $bgTextColor = '';
    /**
     * @var string|null
     */
    private $textColor;
    /**
     * @var string
     */
    private $borderColor;
    /**
     * @var int|null
     */
    private $topMargin;
    /**
     * @var int|null
     */
    private $bottomMargin;
    /**
     * @var string
     */
    private $fontWeight;

    /**
     * @return void
     * @throws Exception
     */
    public function output(): void
    {
        $this->addEmailFooter();
        parent::output();
    }

    public function startHeader(
        $_header,
        $_input
    ): void {
        $this->setEmailStylingFromClass($_input['class'] ?? null);
        switch($_header) {
            case 'h1':
                $fontSize = '36px';
                break;
            case 'h2':
                $fontSize = '32px';
                break;
            case 'h3':
                $fontSize = '28px';
                break;
            default:
                $fontSize = '24px';
        };
        $textColor = $this->textColor ?? $this->bgTextColor;
        $this->addTopSpacer();
        $this->viewport[] = '<' .
            $_header .
            ' style="' .
            $textColor .
            ' padding-top: 0;' .
            ' padding-bottom: 0;' .
            ' font-weight: 500;' .
            ' vertical-align: baseline;' .
            ' font-size: ' . $fontSize . ';' .
            ' line-height: 43.2px;' .
            ' margin: 0;"' .
            ' align="left">';
        $this->addBottomSpacer();
    }

    /**
     * @param string|array|null $_text
     * @param string|array|null $_input
     * @return void
     * @throws Exception
     */
    public function addLead(
        $_text,
        $_input = array()
    ): void {
        $this->setEmailStylingFromClass($_input['class'] ?? null);
        $class = 'text-xl';
        if ($_input['class'] ?? false) {
            $_input['class'] .= ' ' . $class;
        } else {
            $_input['class'] = $class;
        }
        $textColor = $this->textColor ?? $this->bgTextColor;
        $_input['style'] = $textColor . $this->fontWeight . 'line-height: 24px; font-size: 20px; line-height: 24px;';
        $this->addTopSpacer();
        $this->addDiv($_text, $_input);
        $this->addBottomSpacer();
        $this->clearEmailStyling();
    }

    public function startCard(array $_input = array()): void {
        $this->setEmailStylingFromClass($_input['class'] ?? null);
    }

    public function endCard(): void {
        $this->clearEmailStyling();
        $this->bgTextColor = '';
    }

    private function clearEmailStyling(): void {
        $this->bgColor = '';
        $this->textColor = null;
        $this->borderColor = '';
        $this->topMargin = null;
        $this->bottomMargin = null;
        $this->fontWeight = '';
    }

    public function startCardBody(): void {
        $this->addTopSpacer();
        $this->viewport[] =  '<table class="card bg-success border-secondary" role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-radius: 6px; border-collapse: separate !important; width: 100%; overflow: hidden; border: 1px' . $this->borderColor . '" '. $this->bgColor . '>
            <tbody>
            <tr>
            <td style="line-height: 24px; font-size: 16px; width: 100%; margin: 0;" align="left"' . $this->borderColor . $this->bgColor . '>
            <table class="card-body" role="presentation" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
            <tr>
            <td style="line-height: 24px; font-size: 16px; width: 100%; margin: 0; padding: 20px;" align="left">';
    }

    public function endCardBody(): void {
        $this->viewport[] = '</td>';
        $this->viewport[] = '</tr>';
        $this->viewport[] = '</tbody>';
        $this->viewport[] = '</table>';
        $this->viewport[] = '</tr>';
        $this->viewport[] = '</tbody>';
        $this->viewport[] = '</table>';
        $this->addBottomSpacer();
    }

    private function setEmailStylingFromClass(?string $_class): void {
        $this->clearEmailStyling();
        if ($_class) {
            $classes = explode(' ', $_class);
            if ($this->hasClass('bg-success', $classes)) {
                $this->setBgColor(self::SUCCESS_BG_COLOUR);
                $this->setBgTextColor(self::SUCCESS_BG_TEXT_COLOUR);
            } elseif ($this->hasClass('bg-light', $classes)) {
                $this->setBgColor(self::LIGHT_BG_COLOUR);
            }

            if ($this->hasClass('border-secondary', $classes) or
                $this->hasClass('border', $classes)) {
                $this->setBorderColor(self::SECONDARY_COLOUR);
            }

            if ($this->hasClass('text-success', $classes)) {
                $this->setTextColor(self::SUCCESS_COLOUR);
            }
            if ($this->hasClass('font-weight-bold', $classes)) {
                $this->setFontWeight(self::FONT_WEIGHT_BOLD);
            }

            if ($this->hasClass('mt-2', $classes)) {
                $this->setTopMargin(2);
            }
            if ($this->hasClass('mt-4', $classes)) {
                $this->setTopMargin(4);
            }
            if ($this->hasClass('mb-2', $classes)) {
                $this->setBottomMargin(2);
            }
            if ($this->hasClass('mb-4', $classes)) {
                $this->setBottomMargin(4);
            }
        }
    }

    private function hasClass(
        string $_needle,
        array $_haystack
    ): bool {
        return in_array($_needle, $_haystack);
    }

    private function setBgColor(string $_color): void {
        $this->bgColor = ' bgcolor="' . $_color . '"';
    }

    private function setBgTextColor(string $_color): void {
        $this->bgTextColor = 'color: ' . $_color .'; ';
    }

    private function setTextColor(string $_color): void {
        $this->textColor = 'color: ' . $_color .'; ';
    }

    private function setFontWeight(string $_fontWeight): void {
        $this->fontWeight = ' ' . $_fontWeight .'; ';
    }

    private function setBorderColor(string $_color): void {
        $this->borderColor = ' solid ' . $_color . ';';
    }

    private function setTopMargin(int $_amount): void {
        $this->topMargin = $_amount;
    }

    private function setBottomMargin(int $_amount): void {
        $this->bottomMargin = $_amount;
    }

    private function addTopSpacer(): void {
        $this->addSpacer($this->topMargin);
        $this->topMargin = null;
    }

    private function addBottomSpacer(): void {
        $this->addSpacer($this->bottomMargin);
        $this->bottomMargin = null;
    }

    private function addSpacer(?int $_amount): void {
        if ($_amount) {
            $pixelHeight = $_amount * 4;
            $this->viewport[] =
                '<table class="s-4 w-full" role="presentation" border="0" cellpadding="0" cellspacing="0" style="width: 100%;" width="100%">' .
                '<tbody>' .
                '<tr>' .
                '<td style="line-height: 8px; font-size: 8px; width: 100%; height: ' . $pixelHeight . 'px; margin: 0;"' .
                ' align="left"' .
                ' width="100%"' .
                ' height="' . $pixelHeight . '">';
            $this->viewport[] = self::WHITESPACE;
            $this->viewport[] = '</td></tr></tbody></table>';
        }
    }

    public function start(?string $_text = null): void
    {
        $this->addEmailHeaderTop();
        if ($_text) {
            $this->viewport[] = '<div style="display:none;">' . $_text . '</div>';
        }
        $this->addEmailHeaderBottom();
    }

    private function addEmailHeaderTop(): void {
        $this->viewport[] = <<<HEREDOC
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
  <head>
    <!-- Based on Bootstrap Email version: 1.3.1 --><meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="x-apple-disable-message-reformatting">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no, date=no, address=no, email=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <style type="text/css">
      body,table,td{font-family:Inter,Helvetica,Arial,sans-serif !important}.ExternalClass{width:100%}.ExternalClass,.ExternalClass p,.ExternalClass span,.ExternalClass font,.ExternalClass td,.ExternalClass div{line-height:150%}a{text-decoration:none}*{color:inherit}a[x-apple-data-detectors],u+#body a,#MessageViewBody a{color:inherit;text-decoration:none;font-size:inherit;font-family:inherit;font-weight:inherit;line-height:inherit}img{-ms-interpolation-mode:bicubic}table:not([class^=s-]){font-family:Helvetica,Arial,sans-serif;mso-table-lspace:0pt;mso-table-rspace:0pt;border-spacing:0px;border-collapse:collapse}table:not([class^=s-]) td{border-spacing:0px;border-collapse:collapse}@media screen and (max-width: 600px){.w-full,.w-full>tbody>tr>td{width:100% !important}*[class*=s-lg-]>tbody>tr>td{font-size:0 !important;line-height:0 !important;height:0 !important}.s-1>tbody>tr>td{font-size:4px !important;line-height:4px !important;height:4px !important}.s-2>tbody>tr>td{font-size:8px !important;line-height:8px !important;height:8px !important}.s-4>tbody>tr>td{font-size:16px !important;line-height:16px !important;height:16px !important}}
      .stamp {
  margin-top: 50px;
  margin-left: 50px;
  position: relative;
  width: 500px;
  height: 300px;
  background: #bbb;
  -webkit-filter: drop-shadow(3px 3px 1px black);
  filter: drop-shadow(0px 0px 5px white);
}
@font-face {
  font-family: 'Inter';
  font-style: normal;
  font-weight: 400;
  font-display: swap;
  src: url(https://fonts.gstatic.com/s/inter/v12/UcCO3FwrK3iLTeHuS_fvQtMwCp50KnMw2boKoduKmMEVuLyfAZ9hjp-Ek-_EeA.woff) format('woff');
  unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF, U+FFFD;
}
    </style>
  </head>
  <body class="bg-white" style="outline: 0; width: 100%; min-width: 100%; height: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 24px; font-weight: normal; font-size: 16px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; color: #000000; margin: 0; padding: 0; border-width: 0;" bgcolor="#ffffff">
HEREDOC;
    }

    private function addEmailHeaderBottom(): void {
        $this->viewport[] = <<<HEREDOC
<table class="bg-white body" valign="top" role="presentation" border="0" cellpadding="0" cellspacing="0" style="outline: 0; width: 100%; min-width: 100%; height: 100%; -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; font-family: Helvetica, Arial, sans-serif; line-height: 24px; font-weight: normal; font-size: 16px; -moz-box-sizing: border-box; -webkit-box-sizing: border-box; box-sizing: border-box; color: #000000; margin: 0; padding: 0; border-width: 0;" bgcolor="#ffffff">
      <tbody>
        <tr>
          <td valign="top" style="line-height: 24px; font-size: 16px; margin: 0;" align="left" bgcolor="#ffffff">
            <table class="container" role="presentation" border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
              <tbody>
                <tr>
                  <td align="center" style="line-height: 24px; font-size: 16px; margin: 0; padding: 0 16px;">
                    <!--[if (gte mso 9)|(IE)]>
                      <table align="center" role="presentation">
                        <tbody>
                          <tr>
                            <td width="600">
                    <![endif]-->
                    <table align="center" role="presentation" border="0" cellpadding="0" cellspacing="0" style="width: 100%; max-width: 600px; margin: 0 auto;">
                      <tbody>
                        <tr>
                          <td style="line-height: 24px; font-size: 16px; margin: 0;" align="left">
HEREDOC;
    }

    private function addEmailFooter(): void {
        $this->viewport[] = <<<HEREDOC
                          </td>
                        </tr>
                      </tbody>
                    </table>
                    <!--[if (gte mso 9)|(IE)]>
                    </td>
                  </tr>
                </tbody>
              </table>
                    <![endif]-->
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>
HEREDOC;
    }
}