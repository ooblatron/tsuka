<?php


namespace Tsuka\Interfaces;


interface Icons
{
    const ICON_HELP = '<i class="fa fa-question-circle"></i>';
    const ICON_SETTINGS = '<i class="fa fa-cog"></i>';
    const ICON_EDIT = '<i class="fa fa-pencil-square-o ml-2"></i>';
    const ICON_EDIT_CLASS = 'fa fa-pencil-square-o ml-2';
    const ICON_PRINT = '<i class="fa fa-print m-2"></i>';
    const ICON_EMAIL = '<i class="fa fa-envelope m-2"></i>';
    const ICON_CALENDAR_CLASS = 'fa fa-calendar';
    const ICON_CALENDAR = '<i class="' . self::ICON_CALENDAR_CLASS . '"></i>';
    const ICON_REMOVE_CLASS = 'fa fa-remove';
    const ICON_REMOVE = '<i class="' . self::ICON_REMOVE_CLASS . '"></i>';
    const ICON_REMOVE_THIN = '&times;';
    const ICON_TICK = '<i class="fa fa-check"></i>';
    const ICON_TICK_THIN = '&#x2713;';
    const ICON_ADD = '<i class="fa fa-plus"></i>';
    const ICON_POSITION = '<i class="fa fa-dot-circle-o"></i>';
    const ICON_TOGGLE_ON = '<i class="fa fa-toggle-on"></i>';
    const ICON_TOGGLE_OFF = '<i class="fa fa-toggle-off"></i>';
    const ICON_ARROW_UP = '<i class="fa fa-arrow-up"></i>';
    const ICON_ARROW_DOWN = '<i class="fa fa-arrow-down"></i>';
    const ICON_INSERT = '<i class="fa fa-arrow-right"></i>';
    const ICON_INSERT_WHITE = '<i class="fa fa-arrow-right text-white"></i>';
    const ICON_INSERT_BELOW = self::ICON_ARROW_DOWN;
    const ICON_QUESTION_MARK = '<i class="fa fa-question"></i>';
    const ICON_DOCUMENT = '<i class="fa fa-file"></i>';
    const ICON_TRANSFER = '<i class="fa fa-exchange"></i>';
    const ICON_DOWNLOAD = '<i class="fa fa-download"></i>';
    const ICON_LOUDHAILER = '<i class="fa fa-bullhorn"></i>';
    const ICON_DELETE = '<i class="fa fa-trash"></i>';
    const ICON_BAN = '<i class="fa fa-ban fa-5x"></i>';
    const ICON_FAST_FORWARD = '<i class="fa fa-fast-forward"></i>';
    const ICON_REWIND = '<i class="fa fa-fast-backward"></i>';
    const ICON_STEP_FORWARD = '<i class="fa fa-step-forward"></i>';
    const ICON_STEP_BACKWARD = '<i class="fa fa-step-backward"></i>';
    const ICON_POINTER = self::ICON_INSERT;
    const ICON_LOCATION = '<i class="fa fa-map-marker"></i>';
    const ICON_VIDEO = '<i class="fa fa-youtube-play"></i>';
    const ICON_LISTEN = '<i class="fa fa-headphones"></i>';
    const ICON_PARTY =  '<i class="fa fa-music"></i>';
    const ICON_EYE =  '<i class="fa fa-eye"></i>';
    const ICON_COMMENT = '<i class="fa fa-comment"></i>';
    const ICON_COMMENT_OUTLINE = '<i class="fa fa-comment-o"';
}