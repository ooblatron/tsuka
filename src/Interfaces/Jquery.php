<?php

namespace Tsuka\Interfaces;

interface Jquery
{
    function exists(): bool;

    function getContent(): string;
}