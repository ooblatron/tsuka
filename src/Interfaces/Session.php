<?php

namespace Tsuka\Interfaces;

interface Session
{
    function getFormErrors(): array;

    function hasFormErrors(): bool;
}