<?php


namespace Tsuka\Interfaces;


interface HtmlEntities
{
    const NBSP = '&nbsp;';
    const NDASH = '&ndash;';
    const MDASH = '&mdash;';
    const CURRENCY = '&pound;';
    const INFO_CIRCLE = '<span data-toggle="tooltip" data-placement="top" title="View player details">&#9432;</span>';
    const APOSTROPHE = '&rsquo;';
    const START_STRIKETHROUGH = '<s>';
    const END_STRIKETHROUGH = '</s>';
    const START_CODE = '<code>';
    const END_CODE = '</code>';
    const WHITESPACE = '&#160;';
}