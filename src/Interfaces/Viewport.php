<?php

namespace Tsuka\Interfaces;

/**
 * Class Viewport
 */
interface Viewport
{
    /**
     * @param string|array|null $_text
     * @param string|array $_input
     * @return void
     */
    public function addParagraph(
        $_text,
        $_input = []
    );

    /**
     * @param array $_input
     */
    public function startParagraph(array $_input = []);

    public function endParagraph();

    /**
     * @param array|string|null $_text
     * @param array|null $_input
     * @return void
     */
    public function addH1(
        $_text,
        ?array $_input = array()
    );

    /**
     * @param array|string|null $_text
     * @param array|null $_input
     * @return void
     */
    public function addH2(
        $_text,
        ?array $_input = array()
    );

    /**
     * @param array|string|null $_text
     * @param array|null $_input
     * @return void
     */
    public function addH3(
        $_text,
        ?array $_input = array()
    );

    /**
     * @param array|string|null $_text
     * @param array|null $_input
     * @return void
     */
    public function addH4(
        $_text,
        ?array $_input = array()
    );

    /**
     * @param array|string|null $_text
     * @param array|null $_input
     * @return void
     */
    public function addH5(
        $_text,
        ?array $_input = array()
    );

    /**
     * @param $_header
     * @param $_input
     */
    public function startHeader($_header, $_input);

    /**
     * @param $_header
     * @param $_input
     * @return void
     */
    public function endHeader($_header, $_input);

    public function addHr();

    public function addListGroup(
        $_input,
        string $_width = ''
    );

    /**
     * @param $_input
     * @return void
     */
    public function addButton($_input);

    /**
     * @param string|array|null $_text
     * @param array $_input
     * @return void
     */
    public function addAlert(
        $_text,
        array $_input = array()
    );

    /**
     * @param string|array|null $_text
     * @param null $_input
     * @return void
     */
    public function addDiv(
        $_text,
        $_input = null
    );

    /**
     * @param array|null $_input
     * @return void
     */
    public function startDiv(array $_input = null);

    public function endDiv();

    /**
     * @param string|array|null $_text
     * @param string $_class
     * @return void
     */
    public function addSpan(
        $_text,
        string $_class = ''
    );

    /**
     * @param string $_class
     */
    public function startSpan(string $_class = '');

    /**
     *
     */
    public function endSpan();

    /**
     * @param string|array|null $_text
     * @param string $_class
     * @return array
     */
    public function span(
        $_text,
        string $_class = ''
    ): array;

    /**
     * @param string $_width
     * @param string $_padding
     */
    public function startCardGroup(string $_width = '', string $_padding = '2');

    public function endCardGroup();

    public function startCardColumns();

    public function endCardColumns();

    /**
     * @param array $_input
     */
    public function startCard(array $_input = array());

    /**
     * End Card
     * @return void
     */
    public function endCard();

    /**
     * @param string $_image
     * @param string|null $_alt
     * @return void
     */
    public function startCardImage(
        string $_image,
        ?string $_alt = ''
    );

    public function endCardImage();

    public function startCardBody();

    public function endCardBody();

    /**
     * @param string|array|null $_text
     * @param array $_input
     * @return void
     */
    public function addCardTitle(
        $_text,
        array $_input = array()
    );

    /**
     * @param array $_input
     */
    public function startCardTitle(array $_input = array());

    /**
     * @param array $_input
     * @return void
     */
    public function endCardTitle(array $_input = array());

    /**
     * @param string|array|null $_text
     */
    public function addCardHeader(
        $_text
    );

    /**
     * @param string|array|null $_text
     * @return void
     */
    public function addCardFooter(
        $_text
    );

    /**
     * @param string $_link
     * @param string|array|null $_text
     * @param string $_class
     * @return void
     */
    public function addCardLink(
        string $_link,
        $_text,
        string $_class = ''
    );

    /**
     * @param string|array|null $_text
     * @param string|null $_class
     * @return void
     */
    public function addCardText(
        $_text,
        ?string $_class = null
    );

    /**
     * @param string|array|null $_text
     * @param bool $_muted
     * @return void
     */
    public function addSmall(
        $_text,
        bool $_muted = true
    );

    /**
     * @param $_input
     * @param bool $_inline
     * @return void
     */
    public function addInlineInputText(
        $_input,
        bool $_inline = true
    );

    /**
     * Start Form Row
     * @param string|null $_class
     * @return void
     */
    public function startFormRow(
        ?string $_class = null
    );

    public function endFormRow();

    /**
     * @param null $_class
     * @return void
     */
    public function startTable($_class = null);

    public function endTable();

    public function startTableRow();

    public function endTableRow();

    /**
     * @param string|array|null $_text
     * @param string|null $_class
     * @param string|null $_colspan
     * @param string|null $_attribute
     * @return void
     */
    public function addTableCell(
        $_text,
        ?string $_class = '',
        ?string $_colspan = '',
        ?string $_attribute = ''
    );

    /**
     * Start Table Cell
     * @param string|null $_class
     * @param string|null $_colspan
     * @param string|null $_attribute
     */
    public function startTableCell(
        ?string $_class = '',
        ?string $_colspan = '',
        ?string $_attribute = ''
    );

    public function endTableCell();

    /**
     * Prepend
     * Prepends the passed viewport to the current viewport. The passed value can either be a viewport object, or a data
     * array that has been extracted from a viewport object.
     *
     * @param $_viewport array|Viewport
     * @return Viewport
     */
    public function prepend($_viewport): Viewport;

    /**
     * Append
     * Appends the passed viewport to the current viewport. The passed value can either be a viewport object, or a data
     * array that has been extracted from a viewport object.
     *
     * @param $_viewport array|Viewport
     * @return Viewport
     */
    public function append($_viewport): Viewport;

    /**
     * Append Once
     * Appends passed out of passed viewport to the current viewport and clears the content of the passed viewport
     * @param Viewport $_viewport
     * @return $this
     */
    public function appendOnce(Viewport $_viewport): Viewport;

    /**
     * @param string $_url
     * @param string|array|null $_text
     * @param array|null $_input
     * @return void
     */
    public function addLink(
        string $_url,
        $_text,
        ?array $_input = []
    );

    /**
     * Add Link Safe
     * @param string $_url
     * @param string|array|null $_text
     * @param array|null $_input
     */
    public function addLinkSafe(
        string $_url,
        $_text,
        ?array $_input = []
    );

    /**
     * Link
     * @param string $_url
     * @param string|array|null $_text
     * @param array|string|null $_input
     * @return array
     */
    public function getLink(
        string $_url,
        $_text,
        $_input = array()
    ): array;

    /**
     * @param string $_url
     * @param string|array|null $_text
     * @param ?array $_input
     * @return array
     */
    public function getLinkSafe(
        string $_url,
        $_text,
        ?array $_input = []
    ): array;

    /**
     * @param string $_link
     * @param string|null $_class
     * @return void
     */
    public function startLink(
        string $_link,
        ?string $_class = null
    );

    /**
     * @return void
     */
    public function endLink();

    /**
     * Start Grid
     * @param string|null $_width
     * @param string|null $_class
     */
    public function startGrid(
        ?string $_width = null,
        ?string $_class = null
    ): void;

    /**
     * End Grid
     * @return void
     */
    public function endGrid(): void;

    /**
     * Start Grid Row
     * @param string|null $_class
     * @return void
     */
    public function startGridRow(?string $_class = null);

    /**
     * @param string|null $_class
     * @return void
     */
    public function startGridRowStriped(?string $_class = null);

    /**
     * @return void
     */
    public function endGridRow();

    /**
     * @param string|array|null $_text
     * @param string|array|null $_width
     * @param ?string $_class
     * @param ?string $_screenWidth
     * @return void
     */
    public function addGridCell(
        $_text,
        $_width = null,
        ?string $_class = null,
        ?string $_screenWidth = null
    );

    /**
     * @param string|array|null $_width
     * @param string|null $_class
     * @param string|null $_screenWidth
     * @return void
     */
    public function startGridCell(
        $_width = null,
        ?string $_class = null,
        ?string $_screenWidth = null
    );

    /**
     *
     * @return void
     */
    public function endGridCell();

    /**
     * @param $_input
     * @return void
     */
    public function addNavigation($_input);

    /**
     * Add Tabs
     * @param $_input
     * @return void
     */
    public function addTabs($_input);

    /**
     * @return void
     */
    public function addTabsFromArray(
        array $_tabs,
        string $_link,
        string $_parameter,
        ?string $_activeTab = null
    );

    /**
     *
     */
    public function startClearfix();

    /**
     * End Clearfix
     */
    public function endClearfix();

    /**
     * @param string|array|null $_left
     * @param string|array|null $_right
     * @param array|null $_left_input
     * @param array|null $_right_input
     * @param string|null $_align
     * @return void
     */
    public function addPair(
        $_left,
        $_right,
        ?array $_left_input = null,
        ?array $_right_input = null,
        ?string $_align = null
    );

    /**
     * @param string|null $_align
     * @return void
     */
    public function startPair(?string $_align = null);

    /**
     * @return void
     */
    public function endPair();

    /**
     * @param string $_id
     * @return void
     */
    public function startModal(string $_id);

    /**
     * End Modal
     * @return void
     */
    public function endModal();

    /**
     * @param string|array|null $_text
     * @param bool $_close
     * @return void
     */
    public function addModalTitle(
        $_text,
        ?bool $_close = true
    );

    /**
     * @return void
     */
    public function startModalBody();

    /**
     * @return void
     */
    public function endModalBody();

    /**
     * @return void
     */
    public function startModalFooter();

    /**
     * @return void
     */
    public function endModalFooter();

    /**
     * @param string|null $_text
     * @return array
     */
    public function code(?string $_text): array;

    /**
     * @param string|null $_string
     * @return string[]
     */
    function strikethrough(?string $_string): array;

    /**
     * @param array|string|null $_text
     * @return string|array|null
     * @return void
     */
    public function getSafe($_text);

    /**
     * @param array $_text
     * @return string|array|null
     * @return string|array|null
     */
    public function getUnpackedSafe(array $_text);

    /**
     * @param array|string $_text
     * @return array
     */
    public function deemSafe($_text): array;

    /**
     * @param $_name
     */
    public function addAnchor($_name);

    /**
     * @param $_url
     * @return void
     */
    public function addQrCode($_url);

    /**
     * @return void
     */
    public function startJumbotron();

    /**
     * @return void
     */
    public function endJumbotron();

    /**
     * Clear
     * Clears the content of this viewport
     */
    public function clear();

    /**
     * Add Pagination Navigation
     * @param array $_list
     * @return void
     */
    public function addPaginationNavigation(array $_list);

    /**
     * @return array
     */
    public function getViewportData(): array;

    /**
     * @return string
     * @return void
     */
    public function getContent(): string;

    /**
     * @return void
     */
    public function display();

    /**
     * @return void
     */
    public function output();

    /**
     *
     */
    public function outputCsv();

    /**
     * Add Image From File
     * From https://stackoverflow.com/a/21600709/2511355
     * Serves an image from a location that is not accessible by the browser.
     * @param string $_path
     * @param string|null $_altText
     * @param array|null $_input
     */
    public function addImageFromFile(
        string $_path,
        ?string $_altText = null,
        ?array $_input = null
    ): void;

    /**
     * Add Image
     * Serves an image from a location that is accessible by the browser.
     * @param string $_path
     * @param string|null $_alt
     * @param array|null $_input
     */
    public function addImage(
        string $_path,
        ?string $_alt = '',
        ?array $_input = array()
    );

    public function addShortcutIconFromFile(string $_filename);

    function getConvertedToId($_label): string;

    function startHtml(): void;

    function endHtml();

    public function addHead(
        string $title,
        ?string $shortcutIconPath = null,
        bool $isOffline = false,
        ?DebugBar $debugBar = null,
        ?string $auxiliaryHeaderScript = null
    ): void;

    function startBody();

    /**
     * @param Jquery|null $_jQuery
     * @param DebugBar|null $_debugBar StandardDebugBar
     * @param bool|null $_useLocal
     */
    function endBody(
        Jquery $_jQuery = null,
        DebugBar $_debugBar = null,
        ?bool $_useLocal = false
    ): void;

    /**
     * @return void
     */
    function addProgressBar(): void;

    /**
     * Add Grid Header
     * E.g.
     * [
     * 'Period'            => ['width' => 1],
     * 'From date'         => ['width' => 2],
     * 'Opening balance'   => ['width' => 2, 'class' => 'text-right']
     * ]
     * @param array $_columnHeadings
     * @param string|null $_class
     * @return void
     */
    function addGridHeader(
        array $_columnHeadings,
        ?string $_class = null
    ): void;

    /**
     * @param array $rows
     * @return void
     */
    function addRowCount(array $rows): void;

    function startNavBar(): void;

    function endNavBar(): void;

    function startNavBarRow(string $_class): void;

    function endNavBarRow(): void;

    function addNavBarBrand(
        string $_label,
        ?string $_imageSource = null
    ): void;

    function startNavBarItems(): void;

    function endNavBarItems(): void;

    /**
     * @param string $_link
     * @param string $_label
     * @return void
     */
    function addNavBarItem(
        string $_link,
        string $_label
    ): void;

    /**
     * @param string $_label
     * @return void
     */
    function startNavBarDropdown(
        string $_label
    ): void;

    /**
     * @return void
     */
    function endNavBarDropdown(): void;

    /**
     * @param string $_link
     * @param string $_label
     * @return void
     */
    function addNavBarDropdownItem(
        string $_link,
        string $_label
    ): void;

    /**
     * @param array $_content
     * @param string|null $_id
     * @return void
     */
    public function addCarousel(
        array $_content,
        ?string $_id = 'carousel'
    ): void;

    /**
     * @return void
     */
    function startList(): void;

    /**
     * @return void
     */
    function endList(): void;

    /**
     * @param string $_text
     * @return void
     */
    function addBullet(string $_text): void;

    /**
     * Add Navbar Search
     * Adds a search field in the navbar
     * @param string $destination
     * @param string $searchParameter
     * @param string $id
     * @param string $placeholder
     * @param string $scriptName
     * @param string $parameterName
     * @return void
     */
    function addNavbarSearch(
        string $destination,
        string $searchParameter,
        string $id,
        string $placeholder,
        string $scriptName,
        string $parameterName
    ): void;

    /**
     * @param string $_title
     * @param string $_link
     * @return void
     */
    function addReportTitleAndDownloadButton(
        string $_title,
        string $_link
    ): void;

    /**
     * @param array|string|null $_text
     * @param array|string|null $_input
     * @return void
     */
    public function addLead(
        $_text,
        $_input = array()
    ): void;

    /**
     * Start
     * This is the screen version of the ViewportEmail start() method
     * @param string $_text
     * @return void
     */
    public function start(string $_text): void;

    /**
     * @param string $_tooltipText
     * @return void
     */
    function startTooltip(string $_tooltipText): void;

    /**
     * @return void
     */
    function endTooltip(): void;

    function startContainer(
        ?string $_class = null,
        ?string $_style = null
    ): void;

    public function endContainer(
        ?string $_type = null
    ): void;
}