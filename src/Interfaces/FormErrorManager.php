<?php

namespace Tsuka\Interfaces;

use Exception;

interface FormErrorManager
{
    public function initialise(): void;

    public function setViewport(Viewport $viewport): void;

    public function setParameterNames(array $parameterNames): void;

    public function setPassedParameters(array $passedParameters): void;

    /*
    |--------------------------------------------------------------------------
    | Validation checking methods
    |--------------------------------------------------------------------------
    |
    | These methods are called by save classes that are checking whether passed
    | parameters are valid.
    |
    */
    /**
     * reset
     * Resets the validator so that old errors are not displayed.
     */
    public function reset(): void;

    /**
     * @param $_rule
     * @param $_param
     * @param $_message string|null
     * @throws Exception
     */
    public function rule($_rule, $_param, ?string $_message = null): void;

    public function errors(): array;

    public function validate(): bool;

    /**
     * @param $_param
     * @param $_errorMessage
     * @throws Exception
     */
    public function addError($_param, $_errorMessage): void;

    /**
     * @param $_formErrors
     */
    public function injectErrors($_formErrors): void;

    /**
     * @param $_param
     * @return Viewport
     * @throws Exception
     */
    public function showError($_param): Viewport;

    /**
     * isInvalid
     * If there are errors, returns whether the supplied parameter is invalid. If there are no errors, returns null.
     * @param string $_param
     * @return bool|null
     * @throws Exception
     */
    public function isInvalid(string $_param): ?bool;

    public function hasErrors(): bool;

    /**
     * @param array|string $_param
     * @param int|null $_index
     * @return string|array|null
     */
    public function oldValue(array|string $_param, ?int $_index = null): array|string|null;
}