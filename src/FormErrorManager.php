<?php


namespace Tsuka;


use Exception;
use Tsuka\Interfaces\Viewport;
use Valitron\Validator;
use function array_merge;
use function call_user_func_array;
use function is_array;
use function is_null;
use function key;
use function reset;

class FormErrorManager
{
    /**
     * @var Validator $validator
     */
    private $validator;
    /**
     * @var array $errors
     */
    private $errors = array();
    /**
     * @var Viewport $viewport
     */
    private $viewport;
    /**
     * @var array $parameterNames
     */
    private $parameterNames = array();
    /**
     * @var array $passedParameters
     */
    private $passedParameters = array();

    public function initialise(): void {
        $this->validator = new Validator();
    }

    public function setViewport(Viewport $viewport) {
        $this->viewport = $viewport;
    }
    
    public function setParameterNames(array $parameterNames) {
        $this->parameterNames = $parameterNames;
    }

    public function setPassedParameters(array $passedParameters) {
        $this->passedParameters = $passedParameters;
    }

    /*
    |--------------------------------------------------------------------------
    | Validation checking methods
    |--------------------------------------------------------------------------
    |
    | These methods are called by save classes that are checking whether passed
    | parameters are valid.
    |
    */
    /**
     * reset
     * Resets the validator so that old errors are not displayed.
     */
    public function reset()
    {
        $this->validator->reset();
    }

    /**
     * @param $_rule
     * @param $_param
     * @param $_message string|null
     * @throws Exception
     */
    public function rule($_rule, $_param, ?string $_message = null)
    {
        if (is_array($_rule)) {
            // todo From PHP 7.3 use array_key_first($_rule)
            // https://stackoverflow.com/a/1028677/2511355
            reset($_rule);
            $firstKey = key($_rule);
        }

        if (is_array($_param)) {
            $ruleParams = array();
            foreach ($_param as $param) {
                $ruleParams [] = $this->getParam($param);
            }
            if (is_array($_rule)) {
                call_user_func_array([$this->validator, 'rule'],
                    array_merge([$firstKey], $ruleParams, current($_rule)));
            } else {
                call_user_func_array([$this->validator, 'rule'], array_merge([$_rule], $ruleParams));
            }
        } else {
            if (is_array($_rule)) {
                $this->validator->rule($firstKey, $this->getParam($_param), current($_rule));
            } else {
                $this->validator->rule($_rule, $this->getParam($_param));
            }
        }

        if (!is_null($_message)) {
            $this->validator->message($_message);
        }
    }

    /**
     * Get Param
     * Gets raw parameter name.
     * E.g.
     * 'in_first_name' => 'p31'
     * 'in_first_name[0]' => 'p31[0]'
     * @throws Exception
     */
    private function getParam(string $_param): string
    {
        if (mb_strpos($_param, '[') === false) { // todo In PHP 8 replace with !str_contains($_params, '[')
            $name = $this->parameterNames[$_param];
        } else {
            $parts = explode('[', $_param);
            $index = explode(']', $parts[1]);
            $name = $this->parameterNames[$parts[0]].'-'.$index[0];
        }

        return $name;
    }

    public function errors(): array
    {
        return $this->errors;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function validate(): bool
    {
        $this->validator = $this->validator->withData($this->getPassedParametersForValidator());
        $validate = $this->validator->validate();
        $this->errors = array_merge($this->errors, $this->validator->errors());
        return $validate;
    }

    /**
     * @param $_param
     * @param $_errorMessage
     * @throws Exception
     */
    public function addError($_param, $_errorMessage)
    {
        $this->errors[$this->parameterNames[$_param]][] = $_errorMessage;
    }

    /**
     * @param $_formErrors
     */
    public function injectErrors($_formErrors)
    {
        $this->errors = $_formErrors;
    }

    /**
     * @param $_param
     * @return Viewport
     * @throws Exception
     */
    public function showError($_param): Viewport
    {
        $displayedError = clone($this->viewport);

        foreach ($this->errors[$this->parameterNames[$_param]] ?? array() as $errorMessage) {
            $displayedError->addAlert($errorMessage, ['type' => 'danger']);
        }

        return $displayedError;
    }

    /**
     * isInvalid
     * If there are errors, returns whether the supplied parameter is invalid. If there are no errors, returns null.
     * @param string $_param
     * @return bool|null
     * @throws Exception
     */
    public function isInvalid(string $_param): ?bool
    {
        $isInvalid = null;
        if ($this->hasErrors()) {
            $isInvalid = isset($this->errors[$this->parameterNames[$_param]]);
        }
        return $isInvalid;
    }

    public function hasErrors(): bool
    {
        return !empty($this->errors);
    }

    /**
     * @param string|array $_param
     * @param int|null $_index
     * @return string|array|null
     */
    public function oldValue($_param, ?int $_index = null)
    {
        if (!is_null($_index)) {
            $value = $this->params->$_param[$_index] ?? null;
        } else {
            $value = $this->params->$_param ?? null;
        }

        return $value;
    }

    private function getPassedParametersForValidator(): array
    {
        $params = array();
        foreach ($this->passedParameters as $param => $value) {
            if (!is_array($value)) {
                $params[$param] = $value;
            } else {
                foreach ($value as $index => $nestedValue) {
                    $params[$param.'-'.$index] = $nestedValue;
                }
            }
        }

        return $params;
    }
}