<?php

use Tsuka\Tests\IconsTest;

require '../../vendor/autoload.php';
$viewport = (new IconsTest())->build();
$viewport->display();