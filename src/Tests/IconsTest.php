<?php

namespace Tsuka\Tests;

use Exception;

class IconsTest extends ViewportTest
{
    /**
     * @throws Exception
     */
    public function build(): ViewportTest {
        $this->startPage('IconsTest');
        $this->addDiv('ICON_HELP');
        $this->addDiv(self::ICON_HELP, ['class' => 'mb-2']);
        $this->addDiv('ICON_SETTINGS');
        $this->addDiv(self::ICON_SETTINGS, ['class' => 'mb-2']);
        $this->addDiv('ICON_EDIT');
        $this->addDiv(self::ICON_EDIT, ['class' => 'mb-2']);
        $this->addDiv('ICON_PRINT');
        $this->addDiv(self::ICON_PRINT, ['class' => 'mb-2']);
        $this->addDiv('ICON_EMAIL');
        $this->addDiv(self::ICON_EMAIL, ['class' => 'mb-2']);
        $this->addDiv('ICON_CALENDAR_CLASS');
        $this->addDiv(self::ICON_CALENDAR, ['class' => 'mb-2']);
        $this->addDiv('ICON_REMOVE');
        $this->addDiv(self::ICON_REMOVE, ['class' => 'mb-2']);
        $this->addDiv('ICON_REMOVE_THIN');
        $this->addDiv(self::ICON_REMOVE_THIN, ['class' => 'mb-2']);
        $this->addDiv('ICON_TICK');
        $this->addDiv(self::ICON_TICK, ['class' => 'mb-2']);
        $this->addDiv('ICON_TICK_THIN');
        $this->addDiv(self::ICON_TICK_THIN, ['class' => 'mb-2']);
        $this->addDiv('ICON_ADD');
        $this->addDiv(self::ICON_ADD, ['class' => 'mb-2']);
        $this->addDiv('ICON_POSITION');
        $this->addDiv(self::ICON_POSITION, ['class' => 'mb-2']);
        $this->addDiv('ICON_TOGGLE_ON');
        $this->addDiv(self::ICON_TOGGLE_ON, ['class' => 'mb-2']);
        $this->addDiv('ICON_TOGGLE_OFF');
        $this->addDiv(self::ICON_TOGGLE_OFF, ['class' => 'mb-2']);
        $this->addDiv('ICON_ARROW_UP');
        $this->addDiv(self::ICON_ARROW_UP, ['class' => 'mb-2']);
        $this->addDiv('ICON_ARROW_DOWN');
        $this->addDiv(self::ICON_ARROW_DOWN, ['class' => 'mb-2']);
        $this->addDiv('ICON_INSERT');
        $this->addDiv(self::ICON_INSERT, ['class' => 'mb-2']);
        $this->addDiv('ICON_INSERT_WHITE');
        $this->addDiv(self::ICON_INSERT_WHITE, ['class' => 'mb-2']);
        $this->addDiv('ICON_INSERT_BELOW');
        $this->addDiv(self::ICON_INSERT_BELOW, ['class' => 'mb-2']);
        $this->addDiv('ICON_QUESTION_MARK');
        $this->addDiv(self::ICON_QUESTION_MARK, ['class' => 'mb-2']);
        $this->addDiv('ICON_DOCUMENT');
        $this->addDiv(self::ICON_DOCUMENT, ['class' => 'mb-2']);
        $this->addDiv('ICON_TRANSFER');
        $this->addDiv(self::ICON_TRANSFER, ['class' => 'mb-2']);
        $this->addDiv('ICON_DOWNLOAD');
        $this->addDiv(self::ICON_DOWNLOAD, ['class' => 'mb-2']);
        $this->addDiv('ICON_LOUDHAILER');
        $this->addDiv(self::ICON_LOUDHAILER, ['class' => 'mb-2']);
        $this->addDiv('ICON_DELETE');
        $this->addDiv(self::ICON_DELETE, ['class' => 'mb-2']);
        $this->addDiv('ICON_BAN');
        $this->addDiv(self::ICON_BAN, ['class' => 'mb-2']);
        $this->addDiv('ICON_FAST_FORWARD');
        $this->addDiv(self::ICON_FAST_FORWARD, ['class' => 'mb-2']);
        $this->addDiv('ICON_REWIND');
        $this->addDiv(self::ICON_REWIND, ['class' => 'mb-2']);
        $this->addDiv('ICON_STEP_FORWARD');
        $this->addDiv(self::ICON_STEP_FORWARD, ['class' => 'mb-2']);
        $this->addDiv('ICON_STEP_BACKWARD');
        $this->addDiv(self::ICON_STEP_BACKWARD, ['class' => 'mb-2']);
        $this->addDiv('ICON_POINTER');
        $this->addDiv(self::ICON_POINTER, ['class' => 'mb-2']);
        $this->addDiv('ICON_LOCATION');
        $this->addDiv(self::ICON_LOCATION, ['class' => 'mb-2']);
        $this->addDiv('ICON_VIDEO');
        $this->addDiv(self::ICON_VIDEO, ['class' => 'mb-2']);
        $this->addDiv('ICON_LISTEN');
        $this->addDiv(self::ICON_LISTEN, ['class' => 'mb-2']);
        $this->addDiv('ICON_PARTY');
        $this->addDiv(self::ICON_PARTY, ['class' => 'mb-2']);
        $this->addDiv('ICON_EYE');
        $this->addDiv(self::ICON_EYE, ['class' => 'mb-2']);
        $this->addDiv('ICON_COMMENT');
        $this->addDiv(self::ICON_COMMENT, ['class' => 'mb-2']);
        $this->addDiv('ICON_COMMENT_OUTLINE');
        $this->addDiv(self::ICON_COMMENT_OUTLINE, ['class' => 'mb-2']);
        $this->endPage();
        return $this;
    }
}